#include "usart.h"

#include "L76C.h"
#include "minmea.h"
#include "pcf8563.h"
#include "simcom.h"

#define GNSS_UART_DEFINE huart2

#define GPS_EXPIRE_TIME (2) // GPS定位过期时间 min

#define GPS_INTERVAL (10)
#define GPS_TIME(n) ((n) / GPS_INTERVAL)

#if 0
#define GPS_DEBUG(...)
#else
#define GPS_DEBUG(...)                                                                                                 \
    do {                                                                                                               \
        debug_printf("[GNSS]");                                                                                        \
        debug_printf(__VA_ARGS__);                                                                                     \
    } while (0)
#endif

typedef struct
{
    const uint8_t *cfg;
    uint16_t size;
} ublox_cfg_t;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 9600 UBX
const uint8_t ublox_cfg_uart[] = { 0xb5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xd0, 0x08, 0x00, 0x00,
                                   0x80, 0x25, 0x00, 0x00, 0x07, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xa2, 0xb5 };
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GNSS 配置
#if 0
// BD
const uint8_t ublox_cfg_gnss[] = { 0xB5, 0x62, 0x06, 0x3E, 0x2C, 0x00, 0x00, 0x00, 0x20, 0x05, 0x00, 0x08, 0x10,
                                   0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01,
                                   0x03, 0x08, 0x10, 0x00, 0x01, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00,
                                   0x00, 0x01, 0x01, 0x06, 0x08, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x01, 0xFC, 0x01 };

#else
// // GPS BD QZSS ABAS
// // 邬工提供
// const uint8_t ublox_cfg_gnss[] = { 0xB5, 0x62, 0x06, 0x3E, 0x2C, 0x00, 0x00, 0x00, 0x20, 0x05, 0x00, 0x08, 0x10,
//                                    0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, 0x01,
//                                    0x03, 0x08, 0x10, 0x00, 0x01, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x01,
//                                    0x00, 0x01, 0x01, 0x06, 0x08, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x01, 0xFF, 0x4D };

// // GPS BD
const uint8_t ublox_cfg_gnss[] = { 0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x08, 0x10, 0x00,
                                   0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x04,
                                   0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x01, 0x00, 0x01, 0x01,
                                   0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00,
                                   0x01, 0x01, 0x06, 0x08, 0x0E, 0x00, 0x00, 0x00, 0x01, 0x01, 0x0D, 0x29 };
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 输出 GBGSV 配置到 GPS BD 模式下不能正常输出 GBGSV 语句
const uint8_t ublox_cfg_gbgsv[] = {
    0xB5, 0x62, 0x06, 0x17, 0x14, 0x00, 0x00, 0x41, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x57
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RATE
#if 0                                                                                                        
const uint8_t ublox_cfg_rate[] = {// 2Hz
    0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xF4, 0x01, 0x01, 0x00, 0x01, 0x00, 0x0B, 0x77
};
#else
const uint8_t ublox_cfg_rate[] = { // 1Hz
    0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xE8, 0x03, 0x01, 0x00, 0x01, 0x00, 0x01, 0x39
};
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PM2
// 校验
// 60s 71 A7
// 120s BC 47
// 180s 08 0A
#if 1
// 60s
const uint8_t ublox_cfg_psm[] = { 0xB5, 0x62, 0x06, 0x3B, 0x30, 0x00, 0x02, 0x06,
                                  0x00, // 花费在捕获模式下的最大时间，设置为0表示由接收器自己决定时间
                                  0x00, 0x00, 0x91, 0x40, 0x01, // 多种标志设置 外部触发 峰值电流 等待时间定位 更新RTC
                                                                // 更新星历 接收器在未定位状态下的行为
                                  0x20, 0x4E, 0x00, 0x00,             // 追踪模式下的更新时间
                                  0x60, 0xEA, 0x00, 0x00,             // 上次捕获失败，捕获重试周期
                                  0x00, 0x00, 0x00, 0x00, 0x01, 0x00, // 追踪状态下的保持时间
                                  0x00, 0x00,                         // 最小搜索时间
                                  0x2C, 0x01, 0x00, 0x00, 0x4F, 0xC1, 0x03, 0x00, 0x87, 0x02, 0x00, 0x00, 0xFF, 0x00,
                                  0x00, 0x00, 0x64, 0x40, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x71, 0xA7 };
#else
// 120s
const uint8_t ublox_cfg_psm[] = { 0xB5, 0x62, 0x06, 0x3B, 0x30, 0x00, 0x02, 0x06,
                                  0x00, // 花费在捕获模式下的最大时间，设置为0表示由接收器自己决定时间
                                  0x00, 0x00, 0x91, 0x40, 0x01, // 多种标志设置 外部触发 峰值电流 等待时间定位 更新RTC
                                                                // 更新星历 接收器在未定位状态下的行为
                                  0x20, 0x4E, 0x00, 0x00,             // 追踪模式下的更新时间
                                  0xC0, 0xD4, 0x01, 0x00,             // 上次捕获失败，捕获重试周期
                                  0x00, 0x00, 0x00, 0x00, 0x01, 0x00, // 追踪状态下的保持时间
                                  0x00, 0x00,                         // 最小搜索时间
                                  0x2C, 0x01, 0x00, 0x00, 0x4F, 0xC1, 0x03, 0x00, 0x87, 0x02, 0x00, 0x00, 0xFF, 0x00,
                                  0x00, 0x00, 0x64, 0x40, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBC, 0x47 };
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RXM
#if 1
// 低功耗模式
const uint8_t ublox_cfg_rxm[] = { 0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x01, 0x22, 0x92 };
#else
// 连续模式
const uint8_t ublox_cfg_rxm[] = { 0xB5, 0x62, 0x06, 0x11, 0x02, 0x00, 0x08, 0x00, 0x21, 0x91 };
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CFG
const uint8_t ublox_cfg_cfg[] = { 0xb5, 0x62, 0x06, 0x09, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF,
                                  0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x1D, 0xAB }; // cfg
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 是否使用低功耗
#if 1
const ublox_cfg_t ublox_cfg[] = {
    { ublox_cfg_uart, sizeof(ublox_cfg_uart) },   { ublox_cfg_gnss, sizeof(ublox_cfg_gnss) },
    { ublox_cfg_gbgsv, sizeof(ublox_cfg_gbgsv) }, { ublox_cfg_rate, sizeof(ublox_cfg_rate) },
    { ublox_cfg_psm, sizeof(ublox_cfg_psm) },     { ublox_cfg_rxm, sizeof(ublox_cfg_rxm) },
    { ublox_cfg_cfg, sizeof(ublox_cfg_cfg) },
};
#else
const ublox_cfg_t ublox_cfg[] = {
    { ublox_cfg_uart, sizeof(ublox_cfg_uart) },   { ublox_cfg_gnss, sizeof(ublox_cfg_gnss) },
    { ublox_cfg_gbgsv, sizeof(ublox_cfg_gbgsv) }, { ublox_cfg_rate, sizeof(ublox_cfg_rate) },
    { ublox_cfg_rxm, sizeof(ublox_cfg_rxm) },     { ublox_cfg_cfg, sizeof(ublox_cfg_cfg) },
};
#endif

static uint8_t txbuff[100], rxbuff[2000];
static fifo_t txfifo, rxfifo;

static void gnss_pwren_ctl(bool lv)
{
    if (lv) {
        HAL_GPIO_WritePin(L76C_PWREN_PIN_GPIO_Port, L76C_PWREN_PIN_Pin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(L76C_PWREN_PIN_GPIO_Port, L76C_PWREN_PIN_Pin, GPIO_PIN_RESET);
    }
}

void gnss_hw_init()
{
    gnss_pwren_ctl(false);

    __HAL_UART_ENABLE_IT(&GNSS_UART_DEFINE, UART_IT_RXNE);
}

void gnss_init()
{
    fifo_init(&txfifo, txbuff, sizeof(txbuff), "gnss tx");
    fifo_init(&rxfifo, rxbuff, sizeof(rxbuff), "gnss rx");

    gnss_hw_init();
}

void gnss_uart_interrupt_handler(void)
{
    uint8_t chByte;

    if (__HAL_UART_GET_FLAG(&GNSS_UART_DEFINE, UART_FLAG_RXNE)) {
        chByte = GNSS_UART_DEFINE.Instance->DR;
        fifo_put(&rxfifo, chByte);
    }

    if (__HAL_UART_GET_FLAG(&GNSS_UART_DEFINE, UART_FLAG_TXE)) {
        if (fifo_get(&txfifo, &chByte)) {
            GNSS_UART_DEFINE.Instance->DR = chByte;
        } else {
            __HAL_UART_DISABLE_IT(&GNSS_UART_DEFINE, UART_IT_TXE);
        }
    }
}

static bool uart_send(uint8_t *buff, uint16_t len)
{
    static uint16_t cnt = 0;

    while (1) {
        if (fifo_put(&txfifo, buff[cnt])) {
            if (++cnt >= len) {
                cnt = 0;
                __HAL_UART_ENABLE_IT(&GNSS_UART_DEFINE, UART_IT_TXE);
                return true;
            }
        } else {
            __HAL_UART_ENABLE_IT(&GNSS_UART_DEFINE, UART_IT_TXE);
            break;
        }
    }

    return false;
}

static bool gps_get_frame(uint8_t *buff, uint16_t size)
{
    uint8_t chByte;
    static uint16_t cnt = 0;

    while (1) {
        if (fifo_get(&rxfifo, &chByte)) {
            buff[cnt++] = chByte;
            if (chByte == '\n') {
                buff[cnt++] = 0;
                cnt = 0;
                return true;
            }
            if (cnt >= size) {
                cnt = 0;
                break;
            }
        } else {
            break;
        }
    }

    return false;
}

struct minmea_sentence_rmc nmea_rmc;
struct minmea_sentence_gga nmea_gga;
struct minmea_sentence_gsv nmea_gsv;
static fsm_rt_t gps_parse()
{
    static uint16_t s_hwTimeCnt = 0;
    enum minmea_sentence_id emSentenceId;
    static char buff[300];

    if (++s_hwTimeCnt >= GPS_TIME(1 * 60 * 1000)) {
        s_hwTimeCnt = 0;
        run_param.gps.lon = 0;
        run_param.gps.lat = 0;
        return fsm_rt_timeout;
    }

    if (!gps_get_frame((uint8_t *)buff, sizeof(buff))) {
        return fsm_rt_on_going;
    }

    emSentenceId = minmea_sentence_id(buff, false);
    switch (emSentenceId) {
        case MINMEA_SENTENCE_RMC: {
            if (minmea_parse_rmc(&nmea_rmc, buff)) {

#if LED_INDICATE
                {
                    static uint8_t s_bIsOne = 1;
                    if (s_bIsOne) {
                        s_bIsOne = 0;
                        HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
                        HAL_Delay(BASE_TIMER_TIME(10));
                        HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
                    }
                }
#endif

                if (nmea_rmc.valid) {
                    float lon = minmea_tocoord(&nmea_rmc.longitude);
                    float lat = minmea_tocoord(&nmea_rmc.latitude);
                    uint16_t speed = (uint16_t)(minmea_tofloat(&nmea_rmc.speed) * 1.852f);
                    uint16_t course = (uint16_t)minmea_tofloat(&nmea_rmc.course);

                    struct tm ttime;
                    time_t tt;
                    struct tm *p;

                    ttime.tm_year = (uint16_t)nmea_rmc.date.year + 2000 - 1900;
                    ttime.tm_mon = nmea_rmc.date.month - 1;
                    ttime.tm_mday = nmea_rmc.date.day;
                    ttime.tm_hour = nmea_rmc.time.hours;
                    ttime.tm_min = nmea_rmc.time.minutes;
                    ttime.tm_sec = nmea_rmc.time.seconds;
                    tt = mktime(&ttime);
                    tt += 8 * 60 * 60; // 时区操作
                    p = localtime(&tt);
                    run_param.gps.dateTime[0] = p->tm_year + 1900 - 2000;
                    run_param.gps.dateTime[1] = p->tm_mon + 1;
                    run_param.gps.dateTime[2] = p->tm_mday;
                    run_param.gps.dateTime[3] = p->tm_hour;
                    run_param.gps.dateTime[4] = p->tm_min;
                    run_param.gps.dateTime[5] = p->tm_sec;

                    GPS_DEBUG("$xxRMC [%02d-%02d-%02d %02d:%02d:%02d] (%f,%f)\r\n", run_param.gps.dateTime[0],
                              run_param.gps.dateTime[1], run_param.gps.dateTime[2], run_param.gps.dateTime[3],
                              run_param.gps.dateTime[4], run_param.gps.dateTime[5], minmea_tocoord(&nmea_rmc.latitude),
                              minmea_tocoord(&nmea_rmc.longitude));

                    // pcf8563_write_time(run_param.gps.dateTime);
                    // GPS_DEBUG("time sync success\r\n");

                    run_param.gps.lon = lon;
                    run_param.gps.lat = lat;
                    run_param.gps.speed = speed;
                    run_param.gps.course = course;

                    run_param.uploadRes.lat = lat;
                    run_param.uploadRes.lon = lon;
                    s_hwTimeCnt = 0;

#if LED_INDICATE
                    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
                    HAL_Delay(BASE_TIMER_TIME(10));
                    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
                    HAL_Delay(BASE_TIMER_TIME(100));
                    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
                    HAL_Delay(BASE_TIMER_TIME(10));
                    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
#endif

                    return fsm_rt_cpl;
                } else {
                    GPS_DEBUG("$xxRMC [%02d-%02d-%02d %02d:%02d:%02d]\r\n", nmea_rmc.date.year, nmea_rmc.date.month,
                              nmea_rmc.date.day, nmea_rmc.time.hours, nmea_rmc.time.minutes, nmea_rmc.time.seconds);
                }
            } else {
                GPS_DEBUG("$xxRMC sentence is not parsed\r\n");
            }
        } break;
        case MINMEA_SENTENCE_GGA:
            // if (minmea_parse_gga(&nmea_gga, buff)) {
            //     if (nmea_gga.fix_quality == 0) {
            //         run_param.gps.sateNum = nmea_gga.satellites_tracked;
            //         GPS_DEBUG("$xxGGA invalid,satenum: %d\r\n", run_param.gps.sateNum);
            //     } else {
            //         run_param.gps.sateNum = nmea_gga.satellites_tracked;
            //         GPS_DEBUG("$xxGGA: fix quality: %d,satenum: %d\r\n", nmea_gga.fix_quality,
            //         run_param.gps.sateNum);
            //     }
            // } else {
            //     GPS_DEBUG("$xxGGA sentence is not parsed\r\n");
            // }
            break;
        case MINMEA_SENTENCE_GSA:

            break;
        case MINMEA_SENTENCE_GLL:

            break;
        case MINMEA_SENTENCE_GST:

            break;
        case MINMEA_SENTENCE_GSV:
            // if (minmea_parse_gsv(&nmea_gsv, buff)) {
            //     GPS_DEBUG("$%c%cGSV: %03d:%03d  %03d:%03d  %03d:%03d  %03d:%03d\r\n", buff[1], buff[2],
            //               nmea_gsv.sats[0].nr, nmea_gsv.sats[0].snr, nmea_gsv.sats[1].nr, nmea_gsv.sats[1].snr,
            //               nmea_gsv.sats[2].nr, nmea_gsv.sats[2].snr, nmea_gsv.sats[3].nr, nmea_gsv.sats[3].snr);
            // } else {
            //     GPS_DEBUG("$xxGSV sentence is not parsed\r\n");
            // }
            break;
        case MINMEA_SENTENCE_VTG:

            break;
        case MINMEA_SENTENCE_ZDA:

            break;
        case MINMEA_INVALID:

            break;

        case MINMEA_UNKNOWN:

            break;
    }

    return fsm_rt_on_going;
}

void gnss_task()
{
    static enum {
        GPS_TASK_START = 0,
        GPS_TASK_WAIT,
        GPS_TASK_CFG,
        GPS_TASK_DLY,

        GPS_TASK_RUN,

        GPS_TASK_NEXT_DLY,
    } s_emState = GPS_TASK_START;
    fsm_rt_t emState;
    static uint32_t s_hwTimeCnt = 0;
    static uint8_t s_chCfgCnt = 0;

    switch (s_emState) {
        case GPS_TASK_START:
            s_emState = GPS_TASK_RUN;
            gnss_pwren_ctl(true);
            // break;
        case GPS_TASK_WAIT:
            if (++s_hwTimeCnt >= GPS_TIME(200)) {
                s_hwTimeCnt = 0;
                s_emState = GPS_TASK_CFG;
            }
            break;
        case GPS_TASK_CFG:
            if (uart_send((uint8_t *)ublox_cfg[s_chCfgCnt].cfg, ublox_cfg[s_chCfgCnt].size)) {
                s_emState = GPS_TASK_DLY;
            }
            break;
        case GPS_TASK_DLY:
            if (++s_hwTimeCnt >= GPS_TIME(200)) {
                s_hwTimeCnt = 0;
                s_emState = GPS_TASK_CFG;
                if (++s_chCfgCnt >= SIZEOF(ublox_cfg)) {
                    s_chCfgCnt = 0;
                    s_emState = GPS_TASK_RUN;
                }
            }
            break;
        case GPS_TASK_RUN:
            emState = gps_parse();
            if (fsm_rt_cpl == emState) {
                s_emState = GPS_TASK_NEXT_DLY;
                gnss_pwren_ctl(false);

                run_param.uploadTaskFlag = 1;
            } else if (fsm_rt_timeout == emState) {
                s_emState = GPS_TASK_NEXT_DLY;
                gnss_pwren_ctl(false);

                run_param.uploadTaskFlag = 1;
            }
            break;
        case GPS_TASK_NEXT_DLY:
#if (defined DEBUG_MODE)
            if (++s_hwTimeCnt >= GPS_TIME(1 * 60 * 1000)) {
                s_hwTimeCnt = 0;
                s_emState = GPS_TASK_START;
            }
#endif
            break;
    }
}
