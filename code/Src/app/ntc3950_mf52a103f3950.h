/******************************************************************************/
#ifndef _NTC3380_H_
#define _NTC3380_H_
/******************************************************************************/
/*
 * --------------------------------------
 * 厂家 时恒
 * 厂家型号 MF52A103F3950(A1)
 * 嘉立创编号 C13879
 * 产品链接 https://item.szlcsc.com/14534.html
 * --------------------------------------
 */
#include <stdint.h>

/******************************************************************************/
#define VOR_BIAS (100) // 10K 偏置电阻
// 下偏置 传感器在下半部分
#define NTC_RATIO(x) ((VOR_##x) / ((VOR_##x) + VOR_BIAS))

#define NTC_AD8(x) (NTC_RATIO(x) * 256)
#define NTC_AD10(x) (NTC_RATIO(x) * 1024)
#define NTC_AD12(x) (NTC_RATIO(x) * 4096)
#define NTC_AD16(x) (NTC_RATIO(x) * 65536)
// 上偏置


/******************************************************************************/
// VOR(K) = Value of resistance 电阻值
#define VOR_N55 739.500
#define VOR_N54 705.664
#define VOR_N53 669.165
#define VOR_N52 631.466
#define VOR_N51 593.686
#define VOR_N50 556.644
#define VOR_N49 520.911
#define VOR_N48 486.858
#define VOR_N47 454.704
#define VOR_N46 424.553
#define VOR_N45 396.426
#define VOR_N44 370.283
#define VOR_N43 346.049
#define VOR_N42 323.623
#define VOR_N41 302.890
#define VOR_N40 283.730
#define VOR_N39 266.022
#define VOR_N38 249.649
#define VOR_N37 234.498
#define VOR_N36 220.466
#define VOR_N35 207.454
#define VOR_N34 195.372
#define VOR_N33 184.139
#define VOR_N32 173.681
#define VOR_N31 163.931
#define VOR_N30 154.827
#define VOR_N29 146.315
#define VOR_N28 138.347
#define VOR_N27 130.877
#define VOR_N26 123.866
#define VOR_N25 117.280
#define VOR_N24 111.084
#define VOR_N23 105.252
#define VOR_N22 99.756
#define VOR_N21 94.573
#define VOR_N20 89.682
#define VOR_N19 85.063
#define VOR_N18 80.699
#define VOR_N17 76.574
#define VOR_N16 72.672
#define VOR_N15 68.982
#define VOR_N14 65.489
#define VOR_N13 62.183
#define VOR_N12 59.052
#define VOR_N11 56.087
#define VOR_N10 53.280
#define VOR_N9 50.620
#define VOR_N8 48.100
#define VOR_N7 45.712
#define VOR_N6 43.450
#define VOR_N5 41.306
#define VOR_N4 39.274
#define VOR_N3 37.349
#define VOR_N2 35.524
#define VOR_N1 33.795
#define VOR_0 32.116
#define VOR_1 30.601
#define VOR_2 29.128
#define VOR_3 27.732
#define VOR_4 26.408
#define VOR_5 25.152
#define VOR_6 23.962
#define VOR_7 22.833
#define VOR_8 21.762
#define VOR_9 20.746
#define VOR_10 19.783
#define VOR_11 18.868
#define VOR_12 18.000
#define VOR_13 17.177
#define VOR_14 16.395
#define VOR_15 15.652
#define VOR_16 14.947
#define VOR_17 14.277
#define VOR_18 13.641
#define VOR_19 13.036
#define VOR_20 12.461
#define VOR_21 11.915
#define VOR_22 11.395
#define VOR_23 10.901
#define VOR_24 10.431
#define VOR_25 10.000
#define VOR_26 9.557
#define VOR_27 9.151
#define VOR_28 8.765
#define VOR_29 8.397
#define VOR_30 8.047
#define VOR_31 7.712
#define VOR_32 7.394
#define VOR_33 7.090
#define VOR_34 6.800
#define VOR_35 6.523
#define VOR_36 6.259
#define VOR_37 6.008
#define VOR_38 5.767
#define VOR_39 5.537
#define VOR_40 5.318
#define VOR_41 5.108
#define VOR_42 4.907
#define VOR_43 4.716
#define VOR_44 4.532
#define VOR_45 4.357
#define VOR_46 4.189
#define VOR_47 4.029
#define VOR_48 3.875
#define VOR_49 3.728
#define VOR_50 3.588
#define VOR_51 3.453
#define VOR_52 3.324
#define VOR_53 3.200
#define VOR_54 3.081
#define VOR_55 2.968
#define VOR_56 2.859
#define VOR_57 2.754
#define VOR_58 2.654
#define VOR_59 2.558
#define VOR_60 2.466
#define VOR_61 2.377
#define VOR_62 2.293
#define VOR_63 2.211
#define VOR_64 2.133
#define VOR_65 2.058
#define VOR_66 1.986
#define VOR_67 1.917
#define VOR_68 1.850
#define VOR_69 1.786
#define VOR_70 1.725
#define VOR_71 1.666
#define VOR_72 1.610
#define VOR_73 1.555
#define VOR_74 1.503
#define VOR_75 1.452
#define VOR_76 1.404
#define VOR_77 1.358
#define VOR_78 1.313
#define VOR_79 1.270
#define VOR_80 1.228
#define VOR_81 1.189
#define VOR_82 1.150
#define VOR_83 1.113
#define VOR_84 1.078
#define VOR_85 1.044
#define VOR_86 1.011
#define VOR_87 0.979
#define VOR_88 0.948
#define VOR_89 0.919
#define VOR_90 0.890
#define VOR_91 0.863
#define VOR_92 0.837
#define VOR_93 0.811
#define VOR_94 0.787
#define VOR_95 0.763
#define VOR_96 0.740
#define VOR_97 0.718
#define VOR_98 0.697
#define VOR_99 0.676
#define VOR_100 0.657
#define VOR_101 0.637
#define VOR_102 0.619
#define VOR_103 0.601
#define VOR_104 0.584
#define VOR_105 0.567
#define VOR_106 0.551
#define VOR_107 0.535
#define VOR_108 0.520
#define VOR_109 0.505
#define VOR_110 0.491
#define VOR_111 0.478
#define VOR_112 0.464
#define VOR_113 0.451
#define VOR_114 0.439
#define VOR_115 0.427
#define VOR_116 0.415
#define VOR_117 0.404
#define VOR_118 0.393
#define VOR_119 0.382
#define VOR_120 0.371
#define VOR_121 0.361
#define VOR_122 0.351
#define VOR_123 0.342
#define VOR_124 0.333
#define VOR_125 0.324
/******************************************************************************/
#endif //_NTC3380_H_
/******************************************************************************/
