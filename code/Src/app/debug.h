/*
 * @Author: wudi
 * @Date: 2018-07-04 15:47:29
 * @Last Modified by:   wudi
 * @Last Modified time: 2018-07-04 15:47:29
 */
#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "app.h"

extern void debug_init(void);

extern void debug_task(void);

extern void debug_send(uint8_t *buff, uint16_t size);
extern void debug_printf(const char *fmt, ...);

#endif
