#include "param.h"
#include "save.h"

#define FLASH_DEBUG(...)                                                                                               \
    do {                                                                                                               \
        debug_printf("[param]");                                                                                       \
        debug_printf(__VA_ARGS__);                                                                                     \
    } while (0)

// 参数存储位置
#define PARAM_SAVE_POSITION_DATA_EEPROM (0)
#define PARAM_SAVE_POSITION_FLASH (1)
#define PARAM_SAVE_POSITION_SPI_FLASH (2)

// TODO 修改参数存储位置
#define PARAM_SAVE_POSITION PARAM_SAVE_POSITION_SPI_FLASH

#if PARAM_SAVE_POSITION == PARAM_SAVE_POSITION_DATA_EEPROM
#define SYS_PARAM_ADDR (uint32_t)(0x80000 + FLASH_BASE)
#define DEVICE_PARAM_ADDR (uint32_t)(0x81800 + FLASH_BASE)
#elif PARAM_SAVE_POSITION == PARAM_SAVE_POSITION_FLASH
#define SYS_PARAM_ADDR (uint32_t)(0x5FE00 + FLASH_BASE)
#define DEVICE_PARAM_ADDR (uint32_t)(0x5FF00 + FLASH_BASE)
#elif PARAM_SAVE_POSITION == PARAM_SAVE_POSITION_SPI_FLASH
#define SYS_PARAM_ADDR (uint32_t)(0)
#define DEVICE_PARAM_ADDR (uint32_t)(4 * 1024)
// 设备 ID 存储在 FLASH 中的固定位置，每次上电从其中读取出ID
#define DEVICE_ID_ADDR (uint32_t)(0x5FF00 + FLASH_BASE)
#endif

// 系统参数表配置
#define SYS_PARAM_VALID (0xaa)
#define SYS_PARAM_VERSION (0)
// 设备参数配
#define DEVICE_PARAM_VALID (0xaa)

extern sys_param_t sys_param;
extern device_param_t device_param;

#if PARAM_SAVE_POSITION == PARAM_SAVE_POSITION_DATA_EEPROM
static void flash_read(uint32_t addr, uint8_t *buff, uint32_t len)
{
    for (uint32_t i = 0; i < len; i++) {
        buff[i] = *(uint8_t *)addr;
        addr += 1;
    }
}

static void flash_write(uint32_t addr, uint8_t *buff, uint32_t len)
{
    HAL_FLASHEx_DATAEEPROM_Unlock();

    HAL_FLASHEx_DATAEEPROM_Erase(FLASH_TYPEPROGRAMDATA_WORD, addr);

    for (uint32_t i = 0; i < len; i++) {
        HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_FASTBYTE, addr, (uint32_t)buff[i]);
        addr += 1;
    }

    HAL_FLASHEx_DATAEEPROM_Lock();
}
#elif PARAM_SAVE_POSITION == PARAM_SAVE_POSITION_FLASH
static void flash_read(uint32_t addr, uint8_t *buff, uint32_t len)
{
    uint32_t *array = (uint32_t *)buff;

    for (uint32_t i = 0; i < len;) {
        array[i] = *(uint32_t *)addr;
        addr += 4;
        i += 4;
    }
}

static void flash_write(uint32_t addr, uint8_t *buff, uint32_t len)
{
    FLASH_EraseInitTypeDef EraseInit;
    uint32_t PageError;
    uint32_t *array = (uint32_t *)buff;

    HAL_FLASH_Unlock();

    EraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
    EraseInit.PageAddress = addr;
    EraseInit.NbPages = 1;
    HAL_FLASHEx_Erase(&EraseInit, &PageError);

    for (uint32_t i = 0; i < len;) {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, array[i]);
        addr += 4;
        i += 4;
    }

    HAL_FLASH_Lock();
}
#elif PARAM_SAVE_POSITION == PARAM_SAVE_POSITION_SPI_FLASH
static void flash_read(uint32_t addr, uint8_t *buff, uint32_t len)
{
    sfud_read(run_param.flash, addr, len, buff);
}

static void flash_write(uint32_t addr, uint8_t *buff, uint32_t len)
{
    sfud_erase(run_param.flash, addr, len);

    sfud_write(run_param.flash, addr, len, buff);
}

static void read_device_id(uint32_t addr, uint8_t *buff, uint32_t len)
{
    for (uint32_t i = 0; i < len; i++) {
        buff[i] = *(uint8_t *)addr;
        addr += 1;
    }
}
#endif

void param_init()
{
    FLASH_DEBUG("read param\r\n");

    flash_read(DEVICE_PARAM_ADDR, (uint8_t *)&device_param, sizeof(device_param));
    if (device_param.isValid != DEVICE_PARAM_VALID) {
        device_param.isValid = DEVICE_PARAM_VALID;

        memset(device_param.gsmIMEI, 0, sizeof(device_param.gsmIMEI));
        memset(device_param.gsmIMSI, 0, sizeof(device_param.gsmIMSI));
        memset(device_param.gsmICCID, 0, sizeof(device_param.gsmICCID));

        flash_write(DEVICE_PARAM_ADDR, (uint8_t *)&device_param, sizeof(device_param));

        FLASH_DEBUG("init device param\r\n");
    }

    flash_read(SYS_PARAM_ADDR, (uint8_t *)&sys_param, sizeof(sys_param_t));
    if (sys_param.isValid != SYS_PARAM_VALID) {
        sys_param.isValid = SYS_PARAM_VALID;
        sys_param.version = SYS_PARAM_VERSION;

        //        strcpy(sys_param.dataAddr, DATA_SERVER_ADDR_DEFAULT);
        //        strcpy(sys_param.dataPort, DATA_SERVER_PORT_DEFAULT);

        flash_write(SYS_PARAM_ADDR, (uint8_t *)&sys_param, sizeof(sys_param_t));

        FLASH_DEBUG("init system param\r\n");

        save_param_clear();
    }

    save_param_search();

    run_param.uploadRes.version = UPLOAD_RES_VERSION;
}

void sys_param_write()
{
    flash_write(SYS_PARAM_ADDR, (uint8_t *)&sys_param, sizeof(sys_param_t));
}

void device_param_write()
{
    flash_write(DEVICE_PARAM_ADDR, (uint8_t *)&device_param, sizeof(device_param));
}
