#include "adc.h"
#include "app.h"
#include "ntc3950_mf52a103f3950.h"

#if 0
#define ANALOG_DEBUG(...)
#else
#define ANALOG_DEBUG(...)                                                                                              \
    do {                                                                                                               \
        debug_printf("[analog]");                                                                                      \
        debug_printf(__VA_ARGS__);                                                                                     \
    } while (0)
#endif

#define NTC(TEMP) NTC_AD12(TEMP)
#define NTCMAX NTC(N50)
#define NTCMIN NTC(110)

const uint16_t Temp_AD_Table[] = {
    NTC(N50), NTC(N49), NTC(N48), NTC(N47), NTC(N46), NTC(N45), NTC(N44), NTC(N43), NTC(N42), NTC(N41), NTC(N40),
    NTC(N39), NTC(N38), NTC(N37), NTC(N36), NTC(N35), NTC(N34), NTC(N33), NTC(N32), NTC(N31), NTC(N30), NTC(N29),
    NTC(N28), NTC(N27), NTC(N26), NTC(N25), NTC(N24), NTC(N23), NTC(N22), NTC(N21), NTC(N20), NTC(N19), NTC(N18),
    NTC(N17), NTC(N16), NTC(N15), NTC(N14), NTC(N13), NTC(N12), NTC(N11), NTC(N10), NTC(N9),  NTC(N8),  NTC(N7),
    NTC(N6),  NTC(N5),  NTC(N4),  NTC(N3),  NTC(N2),  NTC(N1),  NTC(0),   NTC(1),   NTC(2),   NTC(3),   NTC(4),
    NTC(5),   NTC(6),   NTC(7),   NTC(8),   NTC(9),   NTC(10),  NTC(11),  NTC(12),  NTC(13),  NTC(14),  NTC(15),
    NTC(16),  NTC(17),  NTC(18),  NTC(19),  NTC(20),  NTC(21),  NTC(22),  NTC(23),  NTC(24),  NTC(25),  NTC(26),
    NTC(27),  NTC(28),  NTC(29),  NTC(30),  NTC(31),  NTC(32),  NTC(33),  NTC(34),  NTC(35),  NTC(36),  NTC(37),
    NTC(38),  NTC(39),  NTC(40),  NTC(41),  NTC(42),  NTC(43),  NTC(44),  NTC(45),  NTC(46),  NTC(47),  NTC(48),
    NTC(49),  NTC(50),  NTC(51),  NTC(52),  NTC(53),  NTC(54),  NTC(55),  NTC(56),  NTC(57),  NTC(58),  NTC(59),
    NTC(60),  NTC(61),  NTC(62),  NTC(63),  NTC(64),  NTC(65),  NTC(66),  NTC(67),  NTC(68),  NTC(69),  NTC(70),
    NTC(71),  NTC(72),  NTC(73),  NTC(74),  NTC(75),  NTC(76),  NTC(77),  NTC(78),  NTC(79),  NTC(80),  NTC(81),
    NTC(82),  NTC(83),  NTC(84),  NTC(85),  NTC(86),  NTC(87),  NTC(88),  NTC(89),  NTC(90),  NTC(91),  NTC(92),
    NTC(93),  NTC(94),  NTC(95),  NTC(96),  NTC(97),  NTC(98),  NTC(99),  NTC(100), NTC(101), NTC(102), NTC(103),
    NTC(104), NTC(105), NTC(106), NTC(107), NTC(108), NTC(109), NTC(110),
};

static void temperature_calc(uint16_t adc)
{
    int16_t i;
    int16_t var16;
    int16_t var;

    if (adc >= NTCMAX) {
        run_param.uploadRes.temperature = -500;
    }

    if (adc <= NTCMIN) {
        run_param.uploadRes.temperature = 1100;
    }

    do {
        for (i = 0; i < SIZEOF(Temp_AD_Table); i++) {
            if (adc >= Temp_AD_Table[i]) {
                var = i - 50;
                var = var * 10;
                var16 = Temp_AD_Table[i - 1] - Temp_AD_Table[i];
                var16 = var16 / 10;
                var16 = (adc - Temp_AD_Table[i]) / var16;
                run_param.uploadRes.temperature = var - var16;
                break;
            }
        }
    } while (0);
}

void analog_init(void)
{
    uint16_t advref, adtemp, advbat;
    ADC_ChannelConfTypeDef sConfig = { 0 };

    HAL_ADCEx_Calibration_Start(&hadc1);

    // sConfig.Channel = ADC_CHANNEL_9;
    // sConfig.Rank = ADC_REGULAR_RANK_1;
    // sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
    // if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
    //     Error_Handler();
    // }
    // HAL_ADC_Start(&hadc1);
    // HAL_ADC_PollForConversion(&hadc1, 5);
    // advbat = HAL_ADC_GetValue(&hadc1);

    sConfig.Channel = ADC_CHANNEL_VREFINT;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
        Error_Handler();
    }
    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 5);
    advref = HAL_ADC_GetValue(&hadc1);

    sConfig.Channel = ADC_CHANNEL_8;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK) {
        Error_Handler();
    }
    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 5);
    adtemp = HAL_ADC_GetValue(&hadc1);

    HAL_ADC_Stop(&hadc1);

    __HAL_RCC_ADC1_CLK_DISABLE();

    temperature_calc(adtemp);

    // run_param.advrefint = advref;
    // 通过内部参考电压计算出 vref ，因为 vref == vdda == vdd，即为系统电压
    run_param.uploadRes.vsys = 4096 * 120 / ((uint32_t)advref);

    // run_param.uploadRes.vbat = ((uint32_t)advbat) * 133 * run_param.uploadRes.vsys / 4096 / 100;
    run_param.uploadRes.vbat = run_param.uploadRes.vsys;

    ANALOG_DEBUG("advref: %d vref: %.2f vbat: %.2f, temp: %.1f\r\n", advref, run_param.uploadRes.vsys / 100.0,
                 run_param.uploadRes.vbat / 100.0, run_param.uploadRes.temperature / 10.0);
}

void analog_task(void) {}
