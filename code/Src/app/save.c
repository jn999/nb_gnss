#include "save.h"
#include "./crc/crc16_ccitt.h"
#include "./smlee/smlee.h"

/**
 * @ TODO 分区是否合理，目录区是否太小
 * @ SPI FLASH 分区
 * @ 0KB-4KB 系统参数存储区
 * @ 4KB- 8KB 用户参数存储区
 * @ 12KB-16KB 数据目录去
 * @ 100KB-200KB 数据区
 * @ 800KB-1MB   IAP区，可以实现两个固件的更新 或者使用双 bank 模式 200K
 */

// 注意要对齐到 spi flash 擦除最小粒度 一般是 4KByte
#define SPI_FLASH_SIZE (512 * 1024) // SPI FLASH 容量大小

/* 报告数据目录区 与 数据区 定义 */
// 目录区初始地址
#define REPORT_SPI_FLASH_INDEX_START_ADDR (12 * 1024)
// 目录区大小
#define REPORT_SPI_FLASH_INDEX_SIZE (4 * 1024)

// 数据区初始地址
#define REPORT_SPI_FLASH_START_ADDR (100 * 1024)
// 数据区总大小
#define REPORT_SPI_FLASH_SIZE (100 * 1024)
// 每条记录的数据长度
#define REPORT_SPI_FLASH_RECORD_SIZE (128)
// 记录总条数
#define REPORT_SPI_FLASH_RECORD_NUM (REPORT_SPI_FLASH_SIZE / REPORT_SPI_FLASH_RECORD_SIZE)
// 擦除大小
#define REPORT_SPI_FLASH_ERASE_SIZE (4096)
// 擦除大小对应的记录条数
#define REPORT_SPI_FLASH_ERASE_NUM (REPORT_SPI_FLASH_ERASE_SIZE / REPORT_SPI_FLASH_RECORD_SIZE)

static smlee_t smleeReport;
static smlee_index_t smleeReportIndex[2];

#define seReportWriteCnt smleeReportIndex[0]
#define seReportReadCnt smleeReportIndex[1]

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static void report_erase(size_t size)
{
    sfud_erase(run_param.flash, REPORT_SPI_FLASH_INDEX_START_ADDR, size);
}

static void report_write(size_t offset, uint8_t *buff, size_t len)
{
    sfud_write(run_param.flash, REPORT_SPI_FLASH_INDEX_START_ADDR + offset, len, buff);
}

static void report_read(size_t offset, uint8_t *buff, size_t len)
{
    sfud_read(run_param.flash, REPORT_SPI_FLASH_INDEX_START_ADDR + offset, len, buff);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void save_param_clear()
{
    smlee_clear(&smleeReport);
}

void save_param_search()
{
    smlee_search(&smleeReport);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool save_readable()
{
    if (seReportWriteCnt.data != seReportReadCnt.data) {
        return true;
    } else {
        return false;
    }
}

void save_read_cpl()
{
    if (seReportReadCnt.data != seReportWriteCnt.data) {
        if (++seReportReadCnt.data >= REPORT_SPI_FLASH_RECORD_NUM) {
            seReportReadCnt.data = 0;
        }
        smlee_write(&smleeReport, &seReportReadCnt);
    }
}

// AA AA AA AA data crcH crcL
bool save_read(uint8_t *buff, uint16_t bufflen)
{
    uint8_t array[REPORT_SPI_FLASH_RECORD_SIZE];
    const uint8_t buff2[4] = { 0xAA, 0xAA, 0xAA, 0xAA };
    crc_t crc;
    uint32_t addr;

    addr = REPORT_SPI_FLASH_START_ADDR + seReportReadCnt.data * REPORT_SPI_FLASH_RECORD_SIZE;
    sfud_read(run_param.flash, addr, REPORT_SPI_FLASH_RECORD_SIZE, array);
    if (!memcmp(array, buff2, 4)) {
        crc = crc_init();
        crc = crc_update(crc, &array[4], bufflen);
        crc = crc_finalize(crc);
        if (crc == (array[bufflen + 4] << 8) + array[bufflen + 5]) {
            memcpy(buff, &array[4], bufflen);
            return true;
        }
    }

    return false;
}

void save_write(uint8_t *buff, uint16_t bufflen)
{
    uint8_t array[REPORT_SPI_FLASH_RECORD_SIZE];
    crc_t crc;
    uint32_t addr;

    addr = REPORT_SPI_FLASH_START_ADDR + seReportWriteCnt.data * REPORT_SPI_FLASH_RECORD_SIZE;

    array[0] = 0xAA;
    array[1] = 0xAA;
    array[2] = 0xAA;
    array[3] = 0xAA;

    memcpy(&array[4], buff, bufflen);

    crc = crc_init();
    crc = crc_update(crc, &array[4], bufflen);
    crc = crc_finalize(crc);
    array[bufflen + 4] = crc >> 8;
    array[bufflen + 5] = crc;

    // 判断是否是新的擦除地址
    if (addr % REPORT_SPI_FLASH_ERASE_SIZE == 0) {
        if (seReportWriteCnt.data < seReportReadCnt.data) {
            if (seReportReadCnt.data - seReportWriteCnt.data < REPORT_SPI_FLASH_ERASE_NUM) {
                // NOTE 数据已擦除，但是读指针没有偏移到下一个扇区
                seReportReadCnt.data = seReportWriteCnt.data + REPORT_SPI_FLASH_ERASE_NUM;
                if (seReportReadCnt.data >= REPORT_SPI_FLASH_RECORD_NUM) {
                    seReportReadCnt.data = 0;
                }
                smlee_write(&smleeReport, &seReportReadCnt);
                debug_printf("[save] ==============save area full, erase one sectors valid data==============r\n");
            }
        }

        sfud_erase(run_param.flash, addr, REPORT_SPI_FLASH_ERASE_SIZE);
    }

    sfud_write(run_param.flash, addr, REPORT_SPI_FLASH_RECORD_SIZE, array);
    sfud_read(run_param.flash, addr, REPORT_SPI_FLASH_RECORD_SIZE, array);

    if (++seReportWriteCnt.data >= REPORT_SPI_FLASH_RECORD_NUM) {
        seReportWriteCnt.data = 0;
    }
    smlee_write(&smleeReport, &seReportWriteCnt);
}

void save_init()
{
    smlee_init(&smleeReport, REPORT_SPI_FLASH_INDEX_SIZE, report_erase, report_write, report_read);
    smlee_index_init(&smleeReport, smleeReportIndex, sizeof(smleeReportIndex) / sizeof(smleeReportIndex[0]));
}
