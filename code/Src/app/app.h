/*
 * @Author: wudi
 * @Date: 2018-07-04 15:47:48
 * @Last Modified by: wudi
 * @Last Modified time: 2018-08-07 17:43:41
 */
#ifndef __APP_H__
#define __APP_H__

#include "base.h"
#include "main.h"

// APP
#include "debug.h"
#include "dfifo.h"
#include "helper.h"
#include "save.h"
#include "scheduler.h"
#include "sfud.h"

// 版本信息
#define HARDWARE_VERSION "2.0.0"
#define SOFTWARE_VERSION "2.0.0"

// debug 模式定义，可以进行仿真调试
// #define DEBUG_MODE
#define USE_SIMCOM_TASK (1)

// 使用LED对工作状态进行指示
#define LED_INDICATE (1)

// onenet 注册码
#define ONENET_AUTH_CODE "nikoladiAT163"

// 上传的资源版本
#define UPLOAD_RES_VERSION (0)

// 使用 LWM2M OMNA 上传资源
// http://www.openmobilealliance.org/wp/OMNA/LwM2M/LwM2MRegistry.html
#define USE_OMNA_LWM2M (1)

#if USE_OMNA_LWM2M

// LWM2M 上传位置的 3 种方式
#define LWM2M_LOCATION_6_0_1 0
#define LWM2M_LOCATION_3336_5515_5514 1
#define LWM2M_LOCATION_3336_5513_5514 2
#define LWM2M_LOCATION_RESID_LAT_LON LWM2M_LOCATION_3336_5513_5514

#else

#define APP_OBJID 32769
#define APP_RESID_VERSION 4001
#define APP_RESID_VALID 26242
#define APP_RESID_VBAT 26243
#define APP_RESID_VSYS 26244
#define APP_RESID_TEMP 26245
#define APP_RESID_LON 5515
#define APP_RESID_LAT 5514
#define APP_RESID_TIMESTAMP 5518

// 资源字符串长度 ; 号分割
#define APP_RESID_LEN (43)

#endif

// 上传间隔
#define LWM2M_UPLOAD_INTERVAL (5) // 单位 分钟 min

// RTC 唤醒单片机时间
#define RTC_WAKE_TIME (4 * 60) // 单位 秒 min

// 时基定义
#define BASE_TIMER_INTERVAL (1) // 单位 毫秒
#define BASE_TIMER_TIME(__n) ((__n) / BASE_TIMER_INTERVAL)

typedef enum
{
    fsm_rt_err = -1,
    fsm_rt_cpl = 0,
    fsm_rt_timeout,
    fsm_rt_on_going,
} fsm_rt_t;

typedef struct
{
    uint8_t isValid;
    uint8_t version;

    char dataAddr[20]; // 数据服务器地址
    char dataPort[10]; // 数据服务器端口
} sys_param_t;

typedef struct
{
    uint8_t isValid;

    char gsmIMEI[20]; // gsm模块的IMEI,初次使用要从板子上获得，如果以后获得的与保存的不一致，认为是没有认证的设备
    char gsmIMSI[30];
    char gsmICCID[20];
} device_param_t;

typedef struct
{
    uint8_t version;

    uint16_t vbat;
    uint16_t vsys;
    int16_t temperature;
    float lon;
    float lat;
    time_t timestamp;

    uint16_t valid; // 数据有效
} upload_res_t;


typedef struct
{
    struct
    {
        uint8_t valid;       // GPS 有效
        uint8_t dateTime[6]; // GPS 时间
        float lon;           // 经度
        float lat;           // 纬度
        uint16_t speed;      // 速度 km/h
        uint16_t course;     // 角度
        uint8_t sateNum;     // 卫星数目
    } gps;

    uint8_t dateTime[6];

    // int16_t temperature; // 环境温度
    // uint16_t vbat;       // 电池电压
    // uint16_t vref;       // 参考电压

    upload_res_t uploadRes;   // 资源结构体
    uint8_t thisTimeUploaded; // 本次数据已上传

    uint8_t uploadTaskFlag; // 上传任务启动标志

    sfud_flash *flash; // spi flash操作结构体
    uint8_t valid;     // 系统有效
} run_param_t;

extern run_param_t run_param;
extern sys_param_t sys_param;
extern device_param_t device_param;

#endif
