#ifndef __PCF8563_H__
#define __PCF8563_H__

#include <stdint.h>

extern void pcf8563_init(void);
extern void pcf8563_task(void);

extern void pcf8563_write_time(uint8_t *buff);

#endif
