#ifndef __SAVE_H__
#define __SAVE_H__

#include "app.h"

typedef enum
{
    SAVE_WRITE_MODE_REPORT = 0,
    SAVE_WRITE_MODE_ERROR,
    SAVE_WRITE_MODE_LOCKINFO,
    SAVE_WRITE_MODE_CTLCMD,
} save_write_mode_t;

extern void save_init(void);

extern void save_param_clear(void);
extern void save_param_search(void);

extern bool save_readable(void);
extern void save_read_cpl(void);
extern bool save_read(uint8_t *buff, uint16_t bufflen);
extern void save_write(uint8_t *buff, uint16_t bufflen);

#endif
