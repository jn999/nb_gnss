#include "pcf8563.h"
#include "app.h"
#include "softiic.h"

#if 0
#define PCF8563_DEBUG(...)
#else
#define PCF8563_DEBUG(...)                                                                                             \
    do {                                                                                                               \
        debug_printf("[RTC]");                                                                                         \
        debug_printf(__VA_ARGS__);                                                                                     \
    } while (0)
#endif

#define BCD2CONVER(__VALUE, __MAST) ((__VALUE & (__MAST)) >> 4) * 10 + ((__VALUE)&0X0F) // BCD转HEX

#define SCL_H()                                                                                                        \
    do {                                                                                                               \
        HAL_GPIO_WritePin(RTC_CLK_PIN_GPIO_Port, RTC_CLK_PIN_Pin, GPIO_PIN_SET);                                       \
    } while (0)
#define SCL_L()                                                                                                        \
    do {                                                                                                               \
        HAL_GPIO_WritePin(RTC_CLK_PIN_GPIO_Port, RTC_CLK_PIN_Pin, GPIO_PIN_RESET);                                     \
    } while (0)
#define SDA_H()                                                                                                        \
    do {                                                                                                               \
        HAL_GPIO_WritePin(RTC_DAT_PIN_GPIO_Port, RTC_DAT_PIN_Pin, GPIO_PIN_SET);                                       \
    } while (0)
#define SDA_L()                                                                                                        \
    do {                                                                                                               \
        HAL_GPIO_WritePin(RTC_DAT_PIN_GPIO_Port, RTC_DAT_PIN_Pin, GPIO_PIN_RESET);                                     \
    } while (0)
#define SDA_IS_H() (HAL_GPIO_ReadPin(RTC_DAT_PIN_GPIO_Port, RTC_DAT_PIN_Pin) != GPIO_PIN_RESET)

static void clk(uint8_t chByte)
{
    if (chByte) {
        SCL_H();
    } else {
        SCL_L();
    }
}

static void dat(uint8_t chByte)
{
    if (chByte) {
        SDA_H();
    } else {
        SDA_L();
    }
}

static uint8_t get_dat(void)
{
    if (SDA_IS_H()) {
        return 1;
    } else {
        return 0;
    }
}

static void delay()
{
    uint16_t i = 1;
    while (i--)
        ;
}

/**********************************************************************************************************************************************/
softiic_t pcf8563;
#define DEVICE_ADDR (0xA2)

static void pcf8563_hw_init() {}

void pcf8563_write_time(uint8_t *buff)
{
    uint8_t array[7];

    array[6] = (buff[0] / 10 * 16) + (buff[0] % 10);
    array[5] = (buff[1] / 10 * 16) + (buff[1] % 10);
    array[3] = (buff[2] / 10 * 16) + (buff[2] % 10);
    array[2] = (buff[3] / 10 * 16) + (buff[3] % 10);
    array[1] = (buff[4] / 10 * 16) + (buff[4] % 10);
    array[0] = (buff[5] / 10 * 16) + (buff[5] % 10);
    array[4] = 0;
    softiic_write_buff(&pcf8563, 2, 7, array);
}

void pcf8563_read_time(uint8_t *date)
{
    uint8_t buff[7];

    softiic_read_buff(&pcf8563, 2, 7, buff);

    date[5] = BCD2CONVER(buff[0], 0X70); //秒
    date[4] = BCD2CONVER(buff[1], 0X70); //分
    date[3] = BCD2CONVER(buff[2], 0X30); //时

    // date[6] = buff[4];                   //星期
    date[2] = BCD2CONVER(buff[3], 0X30); //日
    date[1] = BCD2CONVER(buff[5], 0X10); //月
    date[0] = BCD2CONVER(buff[6], 0XF0); //年
}

void pcf8563_init()
{
    uint8_t buff[16];

    pcf8563_hw_init();
    softiic_interface_register(&pcf8563, DEVICE_ADDR, clk, dat, get_dat, delay);

#if !(defined DEBUG_MODE)
    // 定时器
    softiic_read_buff(&pcf8563, 0, 16, buff);
    buff[1] = 0x11;           // 脉冲触发 定时器中断
    buff[14] = 0x83;          // 定时器使能 时钟 1Hz-0x82 1/60Hz-0x83
    buff[15] = RTC_WAKE_TIME; // 计时脉冲个数
    softiic_write_buff(&pcf8563, 0, 16, buff);

    pcf8563_read_time(run_param.dateTime);
    PCF8563_DEBUG("20%02d-%02d-%02d %02d:%02d:%02d\r\n", run_param.dateTime[0], run_param.dateTime[1],
                  run_param.dateTime[2], run_param.dateTime[3], run_param.dateTime[4], run_param.dateTime[5]);

#if 0 // LED_INDICATE
    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
    HAL_Delay(BASE_TIMER_TIME(10));
    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
#endif
    // // 测试 standby mode 功耗
    // #if 1
    //     if (!(run_param.dateTime[4] % LWM2M_UPLOAD_INTERVAL == 0x00 && run_param.dateTime[5] <= (RTC_WAKE_TIME + 1)))
    //     {
    //         Sys_EnterStandbyMode();
    //     }
    // #else
    //     Sys_EnterStandbyMode();
    // #endif

    // 测试 standby mode 功耗
#if 0
    Sys_EnterStandbyMode();
#endif

    {
        time_t ts;
        DateTime_Timestamp(run_param.dateTime, &ts);
        run_param.uploadRes.timestamp = ts;
    }
#endif
}

void pcf8563_task()
{
    static uint8_t s_chTimeCnt = 0;

    if (++s_chTimeCnt >= 10) {
        s_chTimeCnt = 0;

        pcf8563_read_time(run_param.dateTime);

        PCF8563_DEBUG("20%02d-%02d-%02d %02d:%02d:%02d\r\n", run_param.dateTime[0], run_param.dateTime[1],
                      run_param.dateTime[2], run_param.dateTime[3], run_param.dateTime[4], run_param.dateTime[5]);
    }
}
