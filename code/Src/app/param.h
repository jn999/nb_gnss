#ifndef __PARAM_H__
#define __PARAM_H__

#include "app.h"

extern void param_init(void);

extern void sys_param_write(void);

extern void device_param_write(void);

#endif
