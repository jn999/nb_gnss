#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "main.h"

void DateTime_Timezone(uint8_t *src, uint8_t *dst, uint8_t timezone)
{
    struct tm ttime;
    time_t tt;
    struct tm *p;

    ttime.tm_year = src[0] + 2000 - 1900;
    ttime.tm_mon = src[1] - 1;
    ttime.tm_mday = src[2];
    ttime.tm_hour = src[3];
    ttime.tm_min = src[4];
    ttime.tm_sec = src[5];
    tt = mktime(&ttime);
    tt += (timezone * 60 * 60); // 时区操作
    p = localtime(&tt);
    dst[0] = p->tm_year + 1900 - 2000;
    dst[1] = p->tm_mon + 1;
    dst[2] = p->tm_mday;
    dst[3] = p->tm_hour;
    dst[4] = p->tm_min;
    dst[5] = p->tm_sec;
}

void DateTime_Timestamp(uint8_t *dateTime, time_t *ts)
{
    struct tm ttime;

    ttime.tm_year = dateTime[0] + 2000 - 1900;
    ttime.tm_mon = dateTime[1] - 1;
    ttime.tm_mday = dateTime[2];
    ttime.tm_hour = dateTime[3];
    ttime.tm_min = dateTime[4];
    ttime.tm_sec = dateTime[5];
    *ts = mktime(&ttime);
}

void Sys_EnterSleepMode(void)
{
    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

void Sys_EnterStopMode(void)
{
    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

void Sys_EnterStandbyMode(void)
{
    HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
    HAL_PWR_EnterSTANDBYMode();
}
