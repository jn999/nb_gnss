/*
 * @Author: wudi
 * @Date: 2018-07-04 15:47:25
 * @Last Modified by:   wudi
 * @Last Modified time: 2018-07-04 15:47:25
 */
#include "debug.h"
#include "usart.h"

#define DEBUG_UART_DEFINE huart3

static uint8_t txbuff[5000], rxbuff[200];
static fifo_t txfifo, rxfifo;

static void debug_hw_init()
{
    __HAL_UART_ENABLE_IT(&DEBUG_UART_DEFINE, UART_IT_RXNE);
}

void debug_init()
{
    fifo_init(&txfifo, txbuff, sizeof(txbuff), "debug tx");
    fifo_init(&rxfifo, rxbuff, sizeof(rxbuff), "debug rx");

    debug_hw_init();
}

void debug_send(uint8_t *buff, uint16_t size)
{
#if 1
    for (uint16_t i = 0; i < size; i++) {
        fifo_put(&txfifo, buff[i]);
    }

    __HAL_UART_ENABLE_IT(&DEBUG_UART_DEFINE, UART_IT_TXE);
#else
    for (uint16_t i = 0; i < size; i++) {
        while (!__HAL_UART_GET_FLAG(&DEBUG_UART_DEFINE, UART_FLAG_TXE)) {
        }
        DEBUG_UART_DEFINE.Instance->DR = *buff++;
    }

#endif
}

void debug_printf(const char *fmt, ...)
{
    char buff[200];
    va_list list;

    va_start(list, fmt);
    vsprintf(buff, fmt, list);
    va_end(list);

    debug_send((uint8_t *)buff, strlen(buff));
}

void debug_uart_interrupt_handler(void)
{
    uint8_t chByte;

    if (__HAL_UART_GET_FLAG(&DEBUG_UART_DEFINE, UART_FLAG_TXE)) {
        if (fifo_get(&txfifo, &chByte)) {
            DEBUG_UART_DEFINE.Instance->DR = chByte;
        } else {
            __HAL_UART_DISABLE_IT(&DEBUG_UART_DEFINE, UART_IT_TXE);
        }
    }

    if (__HAL_UART_GET_FLAG(&DEBUG_UART_DEFINE, UART_FLAG_RXNE)) {
        chByte = DEBUG_UART_DEFINE.Instance->DR;
        fifo_put(&rxfifo, chByte);
    }
}
