#ifndef __HELPER_H__
#define __HELPER_H__

#include <stdint.h>

extern void DateTime_Timezone(uint8_t *scr, uint8_t *dst, uint8_t timezone);
extern void DateTime_Timestamp(uint8_t *dateTime, time_t *ts);

extern void Sys_EnterSleepMode(void);
extern void Sys_EnterStopMode(void);
extern void Sys_EnterStandbyMode(void);

#endif
