/*
 * @Author: wudi
 * @Date: 2018-07-04 15:47:45
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:48:35
 */
#ifndef _BASE_H_
#define _BASE_H_

// C Lib
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


// #define CLR_BIT(var, bit) var &= ~(1 << bit)
// #define SET_BIT(var, bit) var |= (1 << bit)
// #define CPL_BIT(var, bit) var ^= (1 << bit)
// #define GET_BIT(var, bit) (var & (1 << bit))

#define SIZEOF(__st) (sizeof(__st) / sizeof(__st[0]))

// 转化为字符串
#define TO_STR1(n) #n
#define TO_STR(n) TO_STR1(n)

// 结构体偏移计算

#endif
