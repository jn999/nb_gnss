/*
 * @Author: nikoladi
 * @Date: 2018-10-17 10:05:04
 * @Last Modified by: nikoladi
 * @Last Modified time: 2018-10-19 17:06:05
 */
#include "scheduler.h"

typedef struct __scheduler_t
{
    uint16_t time;      // 任务时间计数
    uint16_t period;    // 任务运行周期
    void (*task)(void); // 任务函数

    bool run; // 任务运行标志
} scheduler_t;

static scheduler_t scheduler_task_array[SCHEDULER_TASK_NUM];

void scheduler_init(void)
{
    for (uint8_t i = 0; i < SCHEDULER_TASK_NUM; i++) {
        scheduler_task_array[i].time = 0;
        scheduler_task_array[i].period = 0;
        scheduler_task_array[i].task = NULL;
        scheduler_task_array[i].run = false;
    }
}

void scheduler_loop(void)
{
    for (uint8_t i = 0; i < SCHEDULER_TASK_NUM; i++) {
        if (scheduler_task_array[i].run) {
            if (scheduler_task_array[i].task) {
                scheduler_task_array[i].task();
                scheduler_task_array[i].run = false;
            }
        }
    }
}

void scheduler_handler(void)
{
    for (uint8_t i = 0; i < SCHEDULER_TASK_NUM; i++) {
        if (scheduler_task_array[i].time) {
            scheduler_task_array[i].time--;
        } else {
            scheduler_task_array[i].run = true;
            if (scheduler_task_array[i].period) {
                scheduler_task_array[i].time = scheduler_task_array[i].period;
                scheduler_task_array[i].time--;
            }
        }
    }
}

void scheduler_start(void)
{
    // start
}

bool scheduler_task_add(void (*task)(void), uint16_t delay, uint16_t period)
{
    for (uint8_t i = 0; i < SCHEDULER_TASK_NUM; i++) {
        if (scheduler_task_array[i].task == NULL) {
            scheduler_task_array[i].task = task;
            scheduler_task_array[i].time = delay;
            scheduler_task_array[i].period = period;
            scheduler_task_array[i].run = false;

            return true;
        }
    }

    return false;
}

bool scheduler_task_delet(void (*task)(void))
{
    for (uint8_t i = 0; i < SCHEDULER_TASK_NUM; i++) {
        if (scheduler_task_array[i].task == task) {
            scheduler_task_array[i].task = NULL;
            scheduler_task_array[i].time = 0;
            scheduler_task_array[i].period = 0;
            scheduler_task_array[i].run = false;

            return true;
        }
    }

    return false;
}
