/*
 * @Author: nikoladi
 * @Date: 2018-10-17 10:05:04
 * @Last Modified by: nikoladi
 * @Last Modified time: 2018-10-19 17:06:58
 */
#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

// 任务数目
#define SCHEDULER_TASK_NUM (16)

extern void scheduler_init(void);
extern void scheduler_loop(void);
extern void scheduler_handler(void);
extern void scheduler_start(void);

extern bool scheduler_task_add(void (*task)(void), uint16_t delay, uint16_t period);
extern bool scheduler_task_delet(void (*task)(void));

#endif
