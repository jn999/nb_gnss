/*
 * @Author: wudi
 * @Date: 2018-08-07 13:56:28
 * @Last Modified by: wudi
 * @Last Modified time: 2018-08-07 16:16:32
 */
#ifndef __SMLEE_H__
#define __SMLEE_H__

#include <stddef.h>
#include <stdint.h>

/**
 * 要实现的目标 磨损平衡 CRC校验
 *
 * flash 分区
 * 数据区 log区 IAP区
 * 数据区 又分为 系统参数数据区 用户参数数据区 应用数据区
 * 应用数据区根据应用不同，存储的内容不同，如存储项目可能需要循环存储历史记录等
 *
 *   {"iap_need_copy_app","0"},
 *   {"iap_copy_app_size","0"},
 *   {"stop_in_bootloader","0"},
 *   {"device_id","1"},
 *   {"boot_times","0"},
 *
 *
 * 第一种存储方式, 每个字节单独一个虚拟地址
 * simulation_eeprom 区实际存储结构,结构体内的数据每个字节分配一个地址，数据更改时写入地址和数据一块写入
 * flash 有总的擦除次数，可以大致估算出 flash 寿命
 * |---------------|---------------|---------------|---------------|...---------------|
 * |  Addr |  Data |  Addr |  Data |  Addr |  Data |  Addr |  Data |...  Addr |  Data |
 * |---------------|---------------|---------------|---------------|...---------------|
 * | 1Byte | 1Byte | 1Byte | 1Byte | 1Byte | 1Byte | 1Byte | 1Byte |... 1Byte | 1Byte |
 * |---------------|---------------|---------------|---------------|...---------------|
 *
 * 第二种存储方式,全部用 ascii 存储 为了节约存储空间可能会以 123...789 abc...xyz ABC...XYZ 作为 key
 * 数据有效 a=255\0 数据无效删除 a=\0
 * typedef struct
 * {
 *  char key;        // 键
 *  char *name;      //介绍 为了节省空间放在 flash 内部
 *  char value[10];  //键值
 * } keyvalue_t;
 *
 * 第三种存储方式，采用协议方式 7E开头 7F结尾 中间有地址标识 真实数据 中间数据要转义 用小端存储效果会好一点 低位在前
 * typdef struct
 * {
 *  uint8_t addr;
 *  uint32_t data;
 * }
 * 考虑用链表来实现
 *
 * typdef struct {
 *  uint8_t addr;
 *  uint8_t *data;
 *  uint8_t size;
 *  uint8_t crc8;
 * } sml_data_t;
 *
 * 第四种存储方式，采用定长存储，有 地址 数据 CRC16 校验，这种方式实现起来最简单
 * 地址 1字节 数据 5字节 CRC16 2字节  //总共8字节 可以存储 int 或者 最长 5 byte的数组
 *
 * 地址 2字节 数据 4字节 CRC16 2字节  //总共8字节 可以存储 int 或者 最长 4 byte的数组
 *
 * 全局变量
 * offset se 区内部偏移
 * se_size se 区大小
 * se_offset se 区总偏移
 */

/**
 * 对 index 定义
 */
typedef struct __smlee_index_t
{
    uint16_t addr; // 虚拟地址
    uint32_t data; // NOTE 这里实际是 4 字节

    // smlee_t *smlee;
} smlee_index_t;

/**
 * 对 flash块 定义
 */
typedef struct __smlee_t
{
    size_t offset; // flash块 中偏移
    size_t size;   // flash块 大小

    // flash块 操作函数
    void (*erase)(size_t size);
    void (*write)(size_t offset, uint8_t *buff, size_t len);
    void (*read)(size_t offset, uint8_t *buff, size_t len);

    // 绑定 index数组 信息
    smlee_index_t *aidx;
    size_t aidxsize;
} smlee_t;

extern void smlee_init(smlee_t *smlee,
                       size_t size,
                       void (*erase)(size_t size),
                       void (*write)(size_t offset, uint8_t *buff, size_t len),
                       void (*read)(size_t offset, uint8_t *buff, size_t len));
extern void smlee_index_init(smlee_t *smlee, smlee_index_t *aidx, size_t aidxsize);

extern void smlee_search(smlee_t *smlee);
extern void smlee_clear(smlee_t *smlee);

extern void smlee_write(smlee_t *smlee, smlee_index_t *idx);

#endif
