#ifndef __SMLEE_CFG_H__
#define __SMLEE_CFG_H__

#include "../CRC/crc16_ccitt.h"
#include "debug.h"

#if 0
#define SMLEE_DEBUG(...)
#else
#define SMLEE_DEBUG(...)                                                                                               \
    do {                                                                                                               \
        debug_printf("[smlee]");                                                                                       \
        debug_printf(__VA_ARGS__);                                                                                     \
    } while (0)
#endif

#define SMLEE_DATA_SIZE (4) // 数据字节长度
#define SMLEE_ADDR_SIZE (2) // 地址长度
#define SMLEE_CRC_SIZE (2)  // CRC校验长度

#define SMLEE_INDEX_SIZE (SMLEE_DATA_SIZE + SMLEE_ADDR_SIZE + SMLEE_CRC_SIZE)

#endif
