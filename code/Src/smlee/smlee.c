/*
 * @Author: wudi
 * @Date: 2018-08-07 14:09:29
 * @Last Modified by: wudi
 * @Last Modified time: 2018-08-07 17:49:01
 */
#include "smlee.h"
#include "smlee_cfg.h"

/**
 * @brief  对 flash块 进行注册,可能会有多个 flash 块
 * @note
 * @param  *smlee:
 * @param  size:
 * @param  smlee flash 块描述结构体
 *         size  flash 块内的总大小
 *         erase flash 擦除函数
 *         write flash 写函数
 *         read  flash 读函数
 * @retval None
 */
void smlee_init(smlee_t *smlee,
                size_t size,
                void (*erase)(size_t size),
                void (*write)(size_t offset, uint8_t *buff, size_t len),
                void (*read)(size_t offset, uint8_t *buff, size_t len))
{
    smlee->offset = 0;
    smlee->erase = erase;
    smlee->write = write;
    smlee->read = read;
    smlee->size = size;
}

/**
 * @brief  flash块 描述结构体绑定 index数组，并对 index数组 初始化
 * @note
 * @param  *smlee: flash块 描述结构体
 * @param  *aidx: index数组
 * @param  aidxsize: index数组 大小
 * @retval None
 */
void smlee_index_init(smlee_t *smlee, smlee_index_t *aidx, size_t aidxsize)
{
    smlee->aidx = aidx;
    smlee->aidxsize = aidxsize;
    for (size_t i = 0; i < aidxsize; i++) {
        aidx[i].addr = i;
        aidx[i].data = 0;
    }
}

/**
 * @brief  对 flash块 清除
 * @note
 * @param  *smlee:
 * @retval None
 */
void smlee_clear(smlee_t *smlee)
{
    uint8_t buff[SMLEE_INDEX_SIZE];
    crc_t crc;
    smlee_index_t *aidx = smlee->aidx;

    smlee->erase(smlee->size);
    smlee->offset = 0;

    for (size_t i = 0; i < smlee->aidxsize; i++) {
        buff[0] = aidx[i].addr >> 8;
        buff[1] = aidx[i].addr;
        buff[2] = aidx[i].data >> 24;
        buff[3] = aidx[i].data >> 16;
        buff[4] = aidx[i].data >> 8;
        buff[5] = aidx[i].data;

        crc = crc_init();
        crc = crc_update(crc, buff, SMLEE_INDEX_SIZE - SMLEE_CRC_SIZE);
        crc = crc_finalize(crc);
        buff[6] = crc >> 8;
        buff[7] = crc;

        smlee->write(smlee->offset, (uint8_t *)buff, sizeof(buff));
        smlee->offset += sizeof(buff);
    }
}

/**
 * @brief  搜索 flash块 ，并填充 index数组
 * @note
 * @param  *smlee:
 * @retval None
 */
void smlee_search(smlee_t *smlee)
{
    uint8_t buff[SMLEE_INDEX_SIZE];
    crc_t crc;
    uint16_t addr;
    uint32_t data;
    smlee_index_t *aidx = smlee->aidx;

    for (size_t i = 0; i < smlee->size;) {
        smlee->read(i, buff, sizeof(buff));
        addr = (buff[0] << 8) + buff[1];
        if (addr != 0xffff) {
            if (addr < smlee->aidxsize) {
                crc = crc_init();
                crc = crc_update(crc, buff, SMLEE_INDEX_SIZE - SMLEE_CRC_SIZE);
                crc = crc_finalize(crc);
                if (crc == ((buff[6] << 8) + buff[7])) {
                    data = (buff[2] << 24) + (buff[3] << 16) + (buff[4] << 8) + buff[5];
                    aidx[addr].addr = addr | 0x8000;
                    aidx[addr].data = data;
                }
            }
        } else {
            smlee->offset = i;
            break;
        }

        i += sizeof(buff);
    }

    // 判断哪些字段没有从 flash 更新
    SMLEE_DEBUG("search true @@@@@@@ offset:%08X\r\n", smlee->offset);
    for (size_t i = 0; i < smlee->aidxsize; i++) {
        if (aidx[i].addr & 0x8000) {
            aidx[i].addr &= 0x7fff;
            SMLEE_DEBUG("search true @@@@@@@ addr:%04X,data:%08X\r\n", aidx[i].addr, aidx[i].data);
        } else {
            SMLEE_DEBUG("search error @@@@@@@ addr:%04X,data:%08X\r\n", aidx[i].addr, aidx[i].data);
        }
    }
}

/**
 * @brief  写一个 index 到 flash块 中
 * @note
 * @param  *smlee:
 * @param  *idx:
 * @retval None
 */
void smlee_write(smlee_t *smlee, smlee_index_t *idx)
{
    uint8_t buff[SMLEE_INDEX_SIZE];
    crc_t crc;

    buff[0] = idx->addr >> 8;
    buff[1] = idx->addr;
    buff[2] = idx->data >> 24;
    buff[3] = idx->data >> 16;
    buff[4] = idx->data >> 8;
    buff[5] = idx->data;

    crc = crc_init();
    crc = crc_update(crc, buff, SMLEE_INDEX_SIZE - SMLEE_CRC_SIZE);
    crc = crc_finalize(crc);
    buff[6] = crc >> 8;
    buff[7] = crc;

    smlee->write(smlee->offset, (uint8_t *)buff, sizeof(buff));

    SMLEE_DEBUG("write addr:%04X,data:%08X,offset:%08X\r\n", idx->addr, idx->data, smlee->offset);

    smlee->offset += sizeof(buff);
    if (smlee->offset >= smlee->size) {
        smlee_clear(smlee);
    }
}
