/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "gpio.h"
#include "iwdg.h"
#include "spi.h"
#include "usart.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "L76C.h"
#include "analog.h"
#include "debug.h"
#include "param.h"
#include "pcf8563.h"
#include "save.h"
#include "scheduler.h"
#include "simcom.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
run_param_t run_param;
sys_param_t sys_param;
device_param_t device_param;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void led_task(void)
{
    static enum {
        LED_TASK_START = 0,
        LED_TASK_WAIT,
        LED_TASK_H,
        LED_TASK_L,
    } s_emState = LED_TASK_START;
    static uint16_t s_hwTimeCnt = 0;

    switch (s_emState) {
        case LED_TASK_START:
            s_emState = LED_TASK_WAIT;
            // break;
        case LED_TASK_WAIT:
            if (++s_hwTimeCnt >= (10 * 1000) / 50) {
                s_hwTimeCnt = 0;
                s_emState = LED_TASK_H;
            }
            break;
        case LED_TASK_H:
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
            s_emState = LED_TASK_L;
            break;
        case LED_TASK_L:
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
            s_emState = LED_TASK_START;
            break;
    }
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
    /* USER CODE BEGIN 1 */
    scheduler_init();
    // TODO
    SystemCoreClockUpdate();
    /* USER CODE END 1 */


    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */

    /* Configure the system clock */
    SystemClock_Config();

    /* USER CODE BEGIN SysInit */
#if BASE_TIMER_INTERVAL == 1
    if (HAL_SYSTICK_Config(SystemCoreClock / (1000U / HAL_TICK_FREQ_1KHZ)) > 0U) {
        return HAL_ERROR;
    }
#else
    if (HAL_SYSTICK_Config(SystemCoreClock / (1000U / HAL_TICK_FREQ_100HZ)) > 0U) {
        return HAL_ERROR;
    }
#endif

#if 0
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_IWDG_Init();
  MX_SPI2_Init();
  /* USER CODE BEGIN 2 */
#endif

    MX_GPIO_Init();
    MX_USART3_UART_Init();

    debug_init();
    debug_printf("\r\nauthor: nikoladi@163.com\r\nhw: %s sw: %s\r\ntime: %s %s\r\n", HARDWARE_VERSION, SOFTWARE_VERSION,
                 __DATE__, __TIME__);

    // 运行到这里表示系统配置没有问题，主要是时钟配置
    pcf8563_init();

    MX_ADC1_Init();
    MX_USART1_UART_Init();
    MX_USART2_UART_Init();
    MX_SPI2_Init();

    if (sfud_init() == SFUD_SUCCESS) {
        run_param.flash = (sfud_flash *)sfud_get_device_table() + 0;
        debug_printf("[SFUD]sfud init success\r\n");
    } else {
        debug_printf("[SFUD]sfud init error\r\n");
        Error_Handler();
    }
    save_init();
    param_init();

    debug_printf("[NB-IoT]IMEI:%s IMSI:%s ICCID:%s\r\n", device_param.gsmIMEI, device_param.gsmIMSI,
                 device_param.gsmICCID);

    gnss_init();
    simcom_init();
    analog_init();

#if 1 // LED_INDICATE
    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
    HAL_Delay(BASE_TIMER_TIME(10));
    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
#endif

#if (defined DEBUG_MODE)
    run_param.uploadTaskFlag = 1;

    scheduler_task_add(pcf8563_task, BASE_TIMER_TIME(103), BASE_TIMER_TIME(100));
    scheduler_task_add(gnss_task, BASE_TIMER_TIME(15), BASE_TIMER_TIME(10));
#if USE_SIMCOM_TASK
    scheduler_task_add(simcom_task, BASE_TIMER_TIME(43), BASE_TIMER_TIME(10));
#endif
#else
    run_param.uploadTaskFlag = 0;

    scheduler_task_add(gnss_task, BASE_TIMER_TIME(15), BASE_TIMER_TIME(10));
#if USE_SIMCOM_TASK
    scheduler_task_add(simcom_task, BASE_TIMER_TIME(43), BASE_TIMER_TIME(10));
#endif
#endif
    // scheduler_task_add(led_task, BASE_TIMER_TIME(0), BASE_TIMER_TIME(50));

    scheduler_start();
    /* USER CODE END 2 */

    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1) {
        /* USER CODE END WHILE */

        /* USER CODE BEGIN 3 */
        scheduler_loop();

#if !(defined DEBUG_MODE)
        Sys_EnterSleepMode();
#endif
    }
    /* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
    RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
    RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

    /** Initializes the CPU, AHB and APB busses clocks
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
        Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
    PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
        Error_Handler();
    }
}

/* USER CODE BEGIN 4 */
void HAL_SYSTICK_Callback(void)
{
    static uint16_t s_hw1sTimeCnt = 0;

    if (++s_hw1sTimeCnt >= BASE_TIMER_TIME(1000)) {
        s_hw1sTimeCnt = 0;

        // HAL_GPIO_TogglePin(LED_PIN_GPIO_Port, LED_PIN_Pin);
    }

    scheduler_handler();
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    
	while(1);

    /* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
