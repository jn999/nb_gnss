/*
 * @Author: wudi
 * @Date: 2018-07-04 15:07:59
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-20 15:21:22
 */
#include "simcom_cb.h"

#include "pcf8563.h"

simcom_rt_t simcom_general_init_error_cb(void)
{
    return simcom_rt_cpl;
}

simcom_rt_t simcom_boot_error_cb(void)
{
    return simcom_rt_cpl;
}

//+QIRDI: 0,1,0,1,32,32
simcom_rt_t simcom_urc_cb(char *buff)
{
#if MODULE_TYPE == MODULE_TYPE_QUECTEL
    // Quectel +QIRDI: <id>,<sc>,<sid>,<num>,<len>,<tlen>
    // +QIRDI: 0,1,0,1,35,35
    if (!strncmp(buff, "+QIRDI:", sizeof("+QIRDI:") - 1)) {
        sscanf(buff, "+QIRDI: %hhu,%hhu,%hhu,%hu,%hu,%hu", &simcom.net.id, &simcom.net.sc, &simcom.net.sid,
               &simcom.net.num, &simcom.net.len, &simcom.net.tlen);

        return simcom_rt_cpl;
    } else if (!strncmp(&buff[3], "CLOSED", sizeof("CLOSED") - 1)) {
        // 链路关闭标志  0 ,CLOSED
        uint8_t num = 0;
        sscanf(buff, "%hhu, CLOSED", &num);
        simcom.tcpudp[num].state = TCP_STATE_CLOSED;
        return simcom_rt_cpl;
    }
#elif MODULE_TYPE == MODULE_TYPE_SIMCOM
    // +MIPLDISCOVER:0,7924,3336
    if (!strncmp(buff, "+MIPLDISCOVER:0", sizeof("+MIPLDISCOVER:0") - 1)) {
        uint16_t num;
        sscanf(buff, "+MIPLDISCOVER:0,%*d,%hu", &num);
#if 0

#if LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_6_0_1
        if (num == 3316) {
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5515_5514
        if (num == 3336) {
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5513_5514
        if (num == 3336) {
#endif

#endif

#if USE_OMNA_LWM2M
        if (++simcom.lwm2m.cnt >= 3) {
#else
        if (++simcom.lwm2m.cnt >= 1) {
#endif
            simcom.lwm2m.action.send = 1;
        }
        return simcom_rt_cpl;
    } else if (!strncmp(buff, "+CTZV:", sizeof("+CTZV:") - 1)) { // +CTZV:19/6/6,17:58:29,+32
        uint8_t dateTime[6], timezone;
        sscanf(buff, "+CTZV:%hhu/%hhu/%hhu,%hhu:%hhu:%hhu,+%hhu", &dateTime[0], &dateTime[1], &dateTime[2],
               &dateTime[3], &dateTime[4], &dateTime[5], &timezone);

        // DateTime_Timezone(dateTime, run_param.dateTime, timezone / 4);
        // pcf8563_write_time(run_param.dateTime);

        pcf8563_write_time(dateTime);
        SIMCOM_INFO("NB-IoT time sync\r\n");
    } else if (!strncmp(buff, "+MIPLEVENT:0,26,", sizeof("+MIPLEVENT:0,26,") - 1)) {
        simcom.lwm2m.notifyCnt++;
    }
#if SIMCOM_ENABLE_FTP
    else if (!strncmp(buff, "+FTPGET: 1", sizeof("+FTPGET: 1") - 1)) {
        uint8_t num = 0xff;
        sscanf(buff, "+FTPGET: 1,%hhu", &num);
        if (num == 1) {
            // 准备接收
            simcom.ftp.action.recv = 1;
        } else if (num == 0) {
            // FTP get 会话成功 结束
            simcom.ftp.action.recv = 0;
            simcom.ftp.get.state = simcom_ftp_state_cpl;
            if (simcom.ftp.get.cplfunc) {
                simcom.ftp.get.cplfunc();
            }
        } else {
            // FTP get 会话发生错误 结束
            simcom.ftp.action.recv = 0;
            simcom.ftp.action.get = 1;
            simcom.ftp.get.state = simcom_ftp_state_err;
        }
        return simcom_rt_cpl;
    } else if (!strncmp(buff, "+FTPPUT: 1", sizeof("+FTPPUT: 1") - 1)) {
        uint8_t num = 0xff;
        sscanf(buff, "+FTPPUT: 1,%hhu", &num);
        if (num == 1) {
            // 等待发送
            simcom.ftp.action.send = 1;
        } else if (num == 0) {
            // FTP put 会话成功 结束
            simcom.ftp.action.send = 0;
            if (simcom.ftp.put.cplfunc) {
                simcom.ftp.put.cplfunc();
            }
            simcom.ftp.put.state = simcom_ftp_state_cpl;
        } else {
            // FTP put 会话发生错误 结束
            simcom.ftp.action.send = 0;
            simcom.ftp.action.put = 1;
            simcom.ftp.put.state = simcom_ftp_state_err;
        }
        return simcom_rt_cpl;
    }
#endif

#endif

    return simcom_rt_on_going;
}

simcom_rt_t simcom_cplcb_tcp_close()
{
    return simcom_rt_cpl;
}

simcom_rt_t simcom_cplcb_tcp_connect()
{
    return simcom_rt_cpl;
}

// +ICCID: 898607b9191791049034
simcom_rt_t simcom_rescb_iccid(char *buff)
{
    if (!strncmp(buff, "+ICCID:", sizeof("+ICCID:") - 1)) {
        memset(simcom.iccid, 0, sizeof(simcom.iccid));
        memcpy(simcom.iccid, &buff[8], 20);

        debug_printf("[NB-IoT]ICCID:%s\r\n", simcom.iccid);
        if (0 != strcmp(simcom.iccid, device_param.gsmICCID)) {
            strcpy(device_param.gsmICCID, simcom.iccid);
            device_param_write();
        }

        return simcom_rt_cpl;
    } else {
        return simcom_rt_err;
    }

    // return simcom_rt_on_going;
}

// 460043587805876
simcom_rt_t simcom_rescb_imsi(char *buff)
{
    if (buff[0] >= '0' && buff[0] <= '9') {
        memset(simcom.imsi, 0, sizeof(simcom.imsi));
        memcpy(simcom.imsi, buff, 15);

        debug_printf("[NB-IoT]IMSI:%s\r\n", simcom.imsi);
        if (0 != strcmp(simcom.imsi, device_param.gsmIMSI)) {
            strcpy(device_param.gsmIMSI, simcom.imsi);
            device_param_write();
        }

        return simcom_rt_cpl;
    } else {
        return simcom_rt_err;
    }

    // return simcom_rt_on_going;
}

// 866956040117869
simcom_rt_t simcom_rescb_imei(char *buff)
{
    if (buff[0] >= '0' && buff[0] <= '9') {
        memset(simcom.imei, 0, sizeof(simcom.imei));
        memcpy(simcom.imei, buff, 15);

        debug_printf("[NB-IoT]IMEI:%s\r\n", simcom.imei);
        if (0 != strcmp(simcom.imei, device_param.gsmIMEI)) {
            strcpy(device_param.gsmIMEI, simcom.imei);
            device_param_write();
        }

        return simcom_rt_cpl;
    } else {
        return simcom_rt_err;
    }

    // return simcom_rt_on_going;
}

//+CPIN: READY
simcom_rt_t simcom_rescb_cpin(char *buff)
{
    if (!strncmp(buff, "+CPIN", sizeof("+CPIN") - 1)) {
        if (!strncmp(buff, "+CPIN:READY", sizeof("+CPIN:READY") - 1)) {
            return simcom_rt_cpl;
        } else {
            return simcom_rt_err;
        }
    }

    return simcom_rt_on_going;
}

//+CSQ: 14,0
simcom_rt_t simcom_rescb_csq(char *buff)
{
    if (!strncmp(buff, "+CSQ: ", sizeof("+CSQ: ") - 1)) {
        if (!strncmp(buff, "+CSQ: 99,99", sizeof("+CSQ: 99,99") - 1)) {
            return simcom_rt_err;
        } else {
            sscanf(buff, "+CSQ: %hhu,", &simcom.rssi);
            SIMCOM_INFO("NB-IoT csq:%d\r\n", simcom.rssi);
            if (simcom.rssi) {
                return simcom_rt_cpl;
            } else {
                return simcom_rt_err;
            }
        }
    }

    return simcom_rt_on_going;
}

//+CGATT:1
simcom_rt_t simcom_rescb_cgatt(char *buff)
{
    if (!strncmp(buff, "+CGATT:", sizeof("+CGATT:") - 1)) {
        if (buff[7] == '1') {
            return simcom_rt_cpl;
        } else {
            return simcom_rt_err;
        }
    }

    return simcom_rt_on_going;
}

//+CREG: 1,1
simcom_rt_t simcom_rescb_creg(char *buff)
{
    if (!strncmp(buff, "+CREG: 0,", sizeof("+CREG: 0,") - 1)) {
        if (buff[9] == '1' || buff[9] == '5') {
            return simcom_rt_cpl;
        } else {
            return simcom_rt_err;
        }
    }

    return simcom_rt_on_going;
}

// +CEREG: 1, 1,"3f27","0578952b",9
simcom_rt_t simcom_rescb_cereg(char *buff)
{
    if (!strncmp(buff, "+CEREG: ", sizeof("+CEREG: ") - 1)) {
        if (buff[11] == '1' || buff[11] == '5') {
            return simcom_rt_cpl;
        } else {
            return simcom_rt_err;
        }
    }

    return simcom_rt_on_going;
}

//+CGREG: 1,2
simcom_rt_t simcom_rescb_cgreg(char *buff)
{
    if (!strncmp(buff, "+CGREG: 0,", sizeof("+CGREG: 0,") - 1)) {
        if (buff[10] == '1' || buff[10] == '5') {
            return simcom_rt_cpl;
        } else {
            return simcom_rt_err;
        }
    }

    return simcom_rt_on_going;
}

simcom_rt_t simcom_rescb_ipaddr(char *buff)
{
    if (buff[0] >= '0' && buff[0] <= '9') {
        memset(simcom.ip, 0, sizeof(simcom.ip));
        strcpy(simcom.ip, buff);

        return simcom_rt_cpl;
    } else {
        return simcom_rt_err;
    }

    // return simcom_rt_on_going;
}

// AT+CIPSTATUS
// OK

// STATE: IP PROCESSING

// C: 0,0,"TCP","119.164.5.209","33044","CONNECTED"
// C: 1,,"","","","INITIAL"
// C: 2,,"","","","INITIAL"
// C: 3,,"","","","INITIAL"
// C: 4,,"","","","INITIAL"
// C: 5,,"","","","INITIAL"
simcom_rt_t simcom_rescb_cipstatus(char *buff)
{
    if (!strncmp(buff, "STATE: ", sizeof("STATE: ") - 1)) {
        if (strncmp(buff, "STATE: IP PROCESSING", sizeof("STATE: IP PROCESSING") - 1) &&
            strncmp(buff, "STATE: IP STATUS", sizeof("STATE: IP STATUS") - 1)) {
            return simcom_rt_err;
        }
    } else if (!strncmp(buff, "C: ", sizeof("C: ") - 1)) {
        char state[20];
        uint8_t num;
        sscanf(buff, "C: %hhu,%*[^,],%*[^,],%*[^,],%*[^,],\"%[^\"]", &num, state);
        if (num < SIMCOM_TCP_NUM) {
            if (!strncmp(state, "CONNECTED", sizeof("CONNECTED") - 1)) {
                simcom.tcpudp[num].state = TCP_STATE_CONNECTED;
            } else {
                simcom.tcpudp[num].state = TCP_STATE_CLOSED;
            }
        }

        if (num == 5) {
            return simcom_rt_cpl;
        }
    }

    return simcom_rt_on_going;
}

// NOTE
// 0, CONNECT OK 应该属于URC不应该在这里处理
// 0, CLOSE OK
// 0, CONNECT OK
simcom_rt_t simcom_rescb_netconnect(char *buff)
{
    if (!strncmp(&buff[3], "CONNECT OK", sizeof("CONNECT OK") - 1)) {
        uint8_t num = 0;
        sscanf(buff, "%hhu,", &num);
        if (simcom.tcpudp[num].used) {
            simcom.tcpudp[num].state = TCP_STATE_CONNECTED;
            simcom.tcpudp[num].action.send = 0;
            simcom.tcpudp[num].action.recv = 0;

            fifo_clear_not_isr(simcom.tcpudp[num].send.fifo);
            fifo_clear_not_isr(simcom.tcpudp[num].recv.fifo);

            simcom.tcpudp[num].send.tlen = 0;
            simcom.tcpudp[num].send.len = 0;
            simcom.tcpudp[num].recv.tlen = 0;
            simcom.tcpudp[num].recv.len = 0;

            simcom.tcpudp[num].reconnect = 1;
        }
        return simcom_rt_cpl;
    } else if (!strncmp(&buff[3], "ALREADY CONNECT", sizeof("ALREADY CONNECT") - 1)) {

    } else if (!strncmp(&buff[3], "CONNECT FAIL", sizeof("CONNECT FAIL"))) {
        return simcom_rt_err;
    }

    return simcom_rt_on_going;
}

// 0, CLOSE OK
simcom_rt_t simcom_rescb_netclose(char *buff)
{
    if (!strncmp(&buff[3], "CLOSE OK", sizeof("CLOSE OK") - 1)) {
        uint8_t num = 0;
        sscanf(buff, "%hhu, CLOSE OK", &num);
        simcom.tcpudp[num].state = TCP_STATE_CLOSED;
        return simcom_rt_cpl;
    } else if (!strncmp(&buff[3], "ERROR", sizeof("ERROR") - 1)) {
        return simcom_rt_cpl;
    }

    return simcom_rt_on_going;
}

#if SIMCOM_ENABLE_FTP
// +FTPSIZE: 1,0,208756
simcom_rt_t simcom_rescb_ftpsize(char *buff)
{
    if (!strncmp(buff, "+FTPSIZE: 1,0,", sizeof("+FTPSIZE: 1,0,") - 1)) {
        uint32_t size;
        sscanf(buff, "+FTPSIZE: 1,0,%u", &size);
        simcom.ftp.get.size = size;

        return simcom_rt_cpl;
    }

    return simcom_rt_on_going;
}
#endif

simcom_rt_t simcom_cplcb_at()
{
    simcom.state = MODULE_STATE_AT;
    // simcom.rssi = 0;

#if LED_INDICATE
    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
    HAL_Delay(BASE_TIMER_TIME(10));
    HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
#endif

    return simcom_rt_cpl;
}

simcom_rt_t simcom_cplcb_cpin()
{
    simcom.state = MODULE_STATE_CPIN;

    return simcom_rt_cpl;
}

simcom_rt_t simcom_cplcb_cgatt()
{
    simcom.state = MODULE_STATE_REGISTER;

    return simcom_rt_cpl;
}
