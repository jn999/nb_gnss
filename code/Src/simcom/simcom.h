/*
 * @Author: wudi
 * @Date: 2018-07-04 15:10:59
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-20 15:20:36
 */
#ifndef __SIMCOM_H__
#define __SIMCOM_H__

// Cellular Module 蜂窝移动模块
// TODO http mqtt 协议实现

#include "simcom_ftp.h"
#include "simcom_net.h"
#include "simcom_http.h"

#include "simcom_type.h"

extern void simcom_init(void);
extern void simcom_task(void);

extern bool simcom_regist_is_cpl(void);

#endif
