/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:15
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-20 15:13:25
 */
#ifndef __SIMCOM_FTP_H__
#define __SIMCOM_FTP_H__

#include "simcom_type.h"

extern void simcom_ftp_register(ftp_data_func getdatafunc,
                                ftp_cpl_func getcplfunc,
                                ftp_data_func putdatafunc,
                                ftp_cpl_func putcplfunc);

extern void simcom_ftp_config(char *server, char *port, char *username, char *password);
extern bool simcom_ftp_get(char *filepath, char *filename);
extern bool simcom_ftp_put(char *filepath, char *filename);

extern int simcom_ftp_get_percent(void);

#endif
