/*
 * @Author: wudi
 * @Date: 2018-07-04 15:10:46
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:36:29
 */
#ifndef __SIMCOM_PORT_H__
#define __SIMCOM_PROT_H__

// 以下函数根据硬件连接和模块型号决定
// 模块电源控制
extern void simcom_power_ctl(bool lv);
// 模块复位控制 当前没使用
extern void simcom_reset_ctl(bool lv);

// 模块pwrkey控制
extern void simcom_pwrkey_ctl(bool lv);
// 模块休眠模式控制 DTR 可以进入退出休眠模式
extern void simcom_dtr_ctl(bool lv);
// 模块状态监测
extern bool simcom_status_check(void);

// 设置串口发送中断
extern void simcom_uart_set_send_int(void);
// 模块硬件相关初始化
extern void simcom_hw_init(void);

#endif
