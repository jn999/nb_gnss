/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:27
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 16:05:06
 */
#ifndef __SIMCOM_NET_H__
#define __SIMCOM_NET_H__

#include "simcom_type.h"

extern void simcom_tcp_register(uint8_t ch, fifo_t *txfifo, fifo_t *rxfifo);

extern void simcom_tcp_connect(uint8_t ch, char *server, char *port);
extern void simcom_tcp_close(uint8_t ch);

extern int simcom_tcp_send(uint8_t ch, uint8_t *buff, uint16_t len);
extern int simcom_tcp_recv(uint8_t ch, uint8_t *buff, uint16_t len);

extern bool simcom_tcp_is_connect(uint8_t ch);
extern bool simcom_tcp_is_reconnect(uint8_t ch);

extern bool simcom_tcp_is_send_cpl(uint8_t ch);

#endif
