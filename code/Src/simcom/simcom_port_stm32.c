/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:33
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:49:47
 */
#include "app.h"
#include "simcom_type.h"

#include "usart.h"

#define SIMCOM_UART_DEFINE huart1

/*- 电源引脚控制 -*/
void simcom_power_ctl(bool lv)
{
    if (lv) {
        HAL_GPIO_WritePin(SGM6603_PWREN_PIN_GPIO_Port, SGM6603_PWREN_PIN_Pin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(SGM6603_PWREN_PIN_GPIO_Port, SGM6603_PWREN_PIN_Pin, GPIO_PIN_RESET);
    }
}

/*- 复位引脚控制 -*/
void simcom_reset_ctl(bool lv)
{
    // if (lv) {
    //     HAL_GPIO_WritePin(M5313_RESET_GPIO_Port, M5313_RESET_Pin, GPIO_PIN_SET);
    // } else {
    //     HAL_GPIO_WritePin(M5313_RESET_GPIO_Port, M5313_RESET_Pin, GPIO_PIN_RESET);
    // }
}

/*- 休眠引脚控制 -*/
void simcom_dtr_ctl(bool lv)
{
    if (lv) {
        
    } else {
        
    }
}

/*- pwrkey引脚控制 -*/
void simcom_pwrkey_ctl(bool lv)
{
    if (lv) {
        HAL_GPIO_WritePin(M5313_RESET_PIN_GPIO_Port, M5313_RESET_PIN_Pin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(M5313_RESET_PIN_GPIO_Port, M5313_RESET_PIN_Pin, GPIO_PIN_RESET);
    }
}

/*- status引脚检测 -*/
bool simcom_status_check()
{
    return true;
}

/*- 硬件初始化包括 串口 中断 控制IO 初始化 -*/
void simcom_hw_init(void)
{
    simcom_power_ctl(false);

    __HAL_UART_ENABLE_IT(&SIMCOM_UART_DEFINE, UART_IT_RXNE);
}

/*- 打开simcom串口发送中断 -*/
void simcom_uart_set_send_int()
{
    __HAL_UART_ENABLE_IT(&SIMCOM_UART_DEFINE, UART_IT_TXE);
}

/*- 发送中断处理函数 -*/
__INLINE void simcom_uart_tx_handler()
{
    uint8_t chByte;

    if (__HAL_UART_GET_FLAG(&SIMCOM_UART_DEFINE, UART_FLAG_TXE)) {
        if (fifo_get(&simcom.txfifo, &chByte)) {
            SIMCOM_UART_DEFINE.Instance->DR = chByte;
        } else {
            __HAL_UART_DISABLE_IT(&SIMCOM_UART_DEFINE, UART_IT_TXE);
        }
    }
}

/*- 接收中断处理函数 -*/
__INLINE void simcom_uart_rx_handler()
{
    uint8_t chByte;

    if (__HAL_UART_GET_FLAG(&SIMCOM_UART_DEFINE, UART_FLAG_RXNE)) {
        chByte = SIMCOM_UART_DEFINE.Instance->DR;
        fifo_put(&simcom.rxfifo, chByte);
    }
}

/*- 中断处理函数 -*/
void simcom_uart_interrupt_handler()
{
    simcom_uart_rx_handler();

    simcom_uart_tx_handler();
}
