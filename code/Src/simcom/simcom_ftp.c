/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:12
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-20 15:13:05
 */
#include "simcom_ftp.h"

void simcom_ftp_register(ftp_data_func getdatafunc,
                         ftp_cpl_func getcplfunc,
                         ftp_data_func putdatafunc,
                         ftp_cpl_func putcplfunc)
{
    simcom.ftp.get.datafunc = getdatafunc;
    simcom.ftp.get.cplfunc = getcplfunc;

    simcom.ftp.put.datafunc = putdatafunc;
    simcom.ftp.put.cplfunc = putcplfunc;
}

void simcom_ftp_config(char *server, char *port, char *username, char *password)
{
    strcpy(simcom.ftp.server, server);
    strcpy(simcom.ftp.port, port);
    strcpy(simcom.ftp.username, username);
    strcpy(simcom.ftp.password, password);

    simcom.ftp.action.config = 1;

    simcom.ftp.get.state = simcom_ftp_state_cpl;
    simcom.ftp.put.state = simcom_ftp_state_cpl;

    simcom.ftp.get.offset = 0;
    simcom.ftp.put.offset = 0;
}

bool simcom_ftp_get(char *filepath, char *filename)
{
    if (simcom_ftp_state_config != simcom.ftp.get.state && simcom_ftp_state_on_going != simcom.ftp.get.state) {
        strcpy(simcom.ftp.get.filepath, filepath);
        strcpy(simcom.ftp.get.filename, filename);

        simcom.ftp.get.state = simcom_ftp_state_config;

        simcom.ftp.get.offset = 0;

        simcom.ftp.action.get = 1;
        return true;
    }

    return false;
}

bool simcom_ftp_put(char *filepath, char *filename)
{
    if (simcom_ftp_state_config != simcom.ftp.put.state && simcom_ftp_state_on_going != simcom.ftp.put.state) {
        strcpy(simcom.ftp.put.filepath, filepath);
        strcpy(simcom.ftp.put.filename, filename);

        simcom.ftp.put.state = simcom_ftp_state_config;

        simcom.ftp.put.offset = 0;

        simcom.ftp.action.put = 1;
        return true;
    }

    return false;
}

int simcom_ftp_get_percent()
{
    if (simcom.ftp.get.state == simcom_ftp_state_on_going) {
        return simcom.ftp.get.offset * 100 / simcom.ftp.get.size;
    }

    return -1;
}
