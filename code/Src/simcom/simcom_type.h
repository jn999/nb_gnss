#ifndef __SIMCOM_TYPE_H__
#define __SIMCOM_TYPE_H__

#include "simcom_cfg.h"

typedef void (*ftp_data_func)(uint8_t *buff, uint16_t len, uint32_t offset);
typedef void (*ftp_cpl_func)(void);

typedef enum
{
    simcom_rt_err = -1,
    simcom_rt_cpl = 0,
    simcom_rt_on_going,
    simcom_rt_timeout,
} simcom_rt_t;

typedef enum
{
    MODULE_STATE_NONE = 0,
    MODULE_STATE_AT,       // AT有响应
    MODULE_STATE_CPIN,     // 已插卡
    MODULE_STATE_REGISTER, // 附着网络
} simcom_state_t;

typedef struct
{
    uint8_t power;       // 电源打开
    uint8_t valid;       // 定位成功
    uint16_t fixtime;    // 定位时间
    uint8_t datetime[6]; // 年月日时分秒
    uint8_t satenum;     // 卫星数目
    float longitude;     // 经度
    float latitude;      // 纬度
    float speed;         // 速度
} simcom_gnss_t;

typedef struct
{
    uint8_t valid;   // LBS经纬度有效
    float longitude; // 经度
    float latitude;  // 纬度
    uint16_t mcc;    // 移动国家码 中国 460
    uint16_t mnc;    // 移动网号   移动 00
    uint16_t lac;    // 位置区码
    uint16_t cid;    // 小区标识
} simcom_lbs_t;

typedef struct
{
    uint8_t charge;   // 是否在充电
    uint8_t percent;  // 电量百分比
    uint16_t voltage; // 电压大小
} simcom_bat_t;

typedef struct
{
    bool used; // 正在使用

    char server[30];
    char port[7];
    char username[20];
    char password[20];

    struct
    {
        char filepath[50];
        char filename[20];

        enum
        {
            simcom_ftp_state_err = -1,
            simcom_ftp_state_cpl = 0,
            simcom_ftp_state_config,
            simcom_ftp_state_on_going,
            simcom_ftp_state_timeout
        } state;

        uint32_t offset;
        uint32_t size;
#if SIMCOM_FTP_USE_DELAY
        uint32_t delay;
#endif

        // uint8_t error;

        // fifo_t *fifo;
        // get put 处理函数
        ftp_data_func datafunc;
        ftp_cpl_func cplfunc;
    } get, put;

    struct
    {
        uint8_t config : 1;
        uint8_t get : 1;
        uint8_t put : 1;
        uint8_t recv : 1;
        uint8_t send : 1;
    } action;
} simcom_ftp_t;

typedef struct
{
    // 是否使用
    uint8_t used : 1;
    uint8_t type : 1; // 0 TCP 1 UDP

    // 链路服务器端口设置
    char server[20];
    char port[7];

    // IO操作
    struct
    {
        uint16_t tlen; // 剩余准备操作缓冲区数据的总长度
        uint16_t len;  // 本次准备操作缓冲区长度

        //缓冲区定义要大
        fifo_t *fifo;
    } send, recv;

    // 链路动作
    // NOTE 避免这种情况发生，recv需要能自增 OK 出现之前又一次接收到链路上的数据
    // +CIPRXGET: 1,0
    // AT+CIPRXGET=2,0,200
    // +CIPRXGET: 2,0,20,0
    // ~€\0JH0001]W?\0\0W~
    // +CIPRXGET: 1,0
    //
    // OK
    struct
    {
        uint8_t connect : 1; // 链路将要连接
        uint8_t close : 1;   // 链路将要关闭
        uint8_t send : 1;    // 链路有要发送的数据
        uint8_t recv : 5;    // 链路接收到数据
    } action;

    // 链路状态
    enum
    {
        TCP_STATE_CLOSED = 0,
        TCP_STATE_CLOSING,
        TCP_STATE_CONNECTING,
        TCP_STATE_CONNECTED,
    } state;

    // 链路重连了
    uint8_t reconnect;
    // TODO 链路故障代码
} simcom_tcpudp_t;

typedef struct
{
    struct
    {
        uint8_t send : 1;
        uint8_t recv : 1;
    } action;

    uint16_t ackid;

    uint16_t notifyCnt;

    uint8_t cnt;
    // uint16_t objbuff[3];
} simcom_lwm2m_t;

typedef struct
{
    simcom_state_t state; // 模块状态

    uint8_t rssi; //信号质量

    char iccid[30];
    char imsi[20]; // 手机卡唯一识别码
    char imei[20]; // 模块唯一识别码

    char ip[20]; // 模块获取的ip地址

    uint16_t cmdTimeCnt; // 周期发送AT命令计数

#if SIMCOM_ENABLE_GNSS
    simcom_gnss_t gnss; // 定位相关信息
#endif

#if SIMCOM_ENABLE_LBS
    simcom_lbs_t lbs; // 基站定位相关信息
#endif

#if SIMCOM_ENABLE_BAT
    simcom_bat_t bat; // 电量相关信息
#endif

#if SIMCOM_ENABLE_FTP
    simcom_ftp_t ftp; // FTP服务器远程升级数据
#endif
    // tcpudp链路
    simcom_tcpudp_t tcpudp[SIMCOM_TCP_NUM];

#if SIMCOM_ENABLE_LWM2M
    simcom_lwm2m_t lwm2m;
#endif

    // 短信定义

    // 串口接收发送缓冲区
    fifo_t txfifo;
    fifo_t rxfifo;

    // 发送命令全局变量
    char cmd[100];
    // tcp 有动作处理的通道记录
    uint8_t channel;

    // 所有链路可以公用一个 buffer 吗
    // 从模块发送或接收到的数据通过此 buffer 发送出去或者填充到相应的 fifo 中
    uint8_t buff[SIMCOM_BUFF_SIZE];
    uint16_t len;
} simcom_t;

extern simcom_t simcom;

#endif
