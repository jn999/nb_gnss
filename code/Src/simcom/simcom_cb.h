/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:04
 * @Last Modified by:   wudi
 * @Last Modified time: 2018-07-04 15:08:04
 */
#ifndef __SIMCOM_CB_H__
#define __SIMCOM_CB_H__

#include "simcom_type.h"

// urc cb
extern simcom_rt_t simcom_urc_cb(char *buff);

extern simcom_rt_t simcom_general_init_error_cb(void);
extern simcom_rt_t simcom_boot_error_cb(void);

// res cb
extern simcom_rt_t simcom_rescb_cpin(char *buff);

extern simcom_rt_t simcom_rescb_imei(char *buff);
extern simcom_rt_t simcom_rescb_imsi(char *buff);
extern simcom_rt_t simcom_rescb_iccid(char *buff);

extern simcom_rt_t simcom_rescb_csq(char *buff);
extern simcom_rt_t simcom_rescb_cgatt(char *buff);
extern simcom_rt_t simcom_rescb_creg(char *buff);
extern simcom_rt_t simcom_rescb_cereg(char *buff);
extern simcom_rt_t simcom_rescb_cgreg(char *buff);
extern simcom_rt_t simcom_rescb_ipaddr(char *buff);

extern simcom_rt_t simcom_rescb_netconnect(char *buff);
extern simcom_rt_t simcom_rescb_netclose(char *buff);
extern simcom_rt_t simcom_rescb_cipstatus(char *buff);

extern simcom_rt_t simcom_rescb_ftpsize(char *buff);

// cpl cb
extern simcom_rt_t simcom_cplcb_at(void);
extern simcom_rt_t simcom_cplcb_cgatt(void);
extern simcom_rt_t simcom_cplcb_cpin(void);
extern simcom_rt_t simcom_cplcb_tcp_close(void);
extern simcom_rt_t simcom_cplcb_tcp_connect(void);

#endif
