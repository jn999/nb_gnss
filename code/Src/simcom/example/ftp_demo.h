/*
 * @Author: wudi
 * @Date: 2018-07-04 15:11:10
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:38:25
 */
#ifndef __FTP_DEMO_H__
#define __FTP_DEMO_H__

#include "simcom.h"

extern void ftp_iap_download(char *addr, char *port, char *username, char *password, char *filepath, char *filename);

#endif
