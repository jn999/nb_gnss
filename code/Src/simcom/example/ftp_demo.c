/*
 * @Author: wudi
 * @Date: 2018-07-04 15:11:07
 * @Last Modified by:   wudi
 * @Last Modified time: 2018-07-04 15:11:07
 */
#include "./ftp_demo.h"
#include "sfud.h"

static void ftp_get_data_func(uint8_t *buff, uint16_t len, uint32_t offset)
{
    if (offset == 0) {
        sfud_erase(run_param.flash, APP_SPI_FLASH_START_ADDR, 256 * 1024);
    }

    sfud_write(run_param.flash, APP_SPI_FLASH_START_ADDR + offset, len, buff);
}

static void ftp_get_cpl_func(void)
{
    // IAP文件前3个字节原始为0xFE 升级文件下载完成以后修改为 0xAA
    uint8_t buff[3] = { 0xAA, 0xAA, 0xAA };
    sfud_write(run_param.flash, APP_SPI_FLASH_START_ADDR, sizeof(buff), buff);

    __set_FAULTMASK(1);
    NVIC_SystemReset();
}

void ftp_iap_download(char *addr, char *port, char *username, char *password, char *filepath, char *filename)
{
    simcom_ftp_config(addr, port, username, password);
    simcom_ftp_get(filepath, filename);

    simcom_ftp_register(ftp_get_data_func, ftp_get_cpl_func, NULL, NULL);
}
