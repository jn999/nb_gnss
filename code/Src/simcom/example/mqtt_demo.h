/*
 * @Author: wudi
 * @Date: 2018-07-04 15:11:21
 * @Last Modified by:   wudi
 * @Last Modified time: 2018-07-04 15:11:21
 */
#ifndef __MQTT_DEMO_H__
#define __MQTT_DEMO_H__

#include "app.h"
#include "simcom.h"

extern void mqtt_init(void);
extern void mqtt_task(void);

#endif
