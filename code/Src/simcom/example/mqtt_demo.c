/*
 * @Author: wudi
 * @Date: 2018-07-04 15:11:18
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:39:44
 */
#include "./mqtt_demo.h"

#define MQTT_SERVER_ADDR "mqtt.heclouds.com"
#define MQTT_SERVER_PORT "6002"
#define MQTT_CLIENTID "35638775"
#define MQTT_USERNAME "154419"
#define MQTT_PASSWORD "DBjyJQkM=o5l5An9rZnPG=WkUt0="

#define MQTT_TCP_NUM (1)

static fifo_t txfifo,rxfifo;
static uint8_t txbuff[200],rxbuff[200];

void mqtt_init()
{
    fifo_init(&txfifo, txbuff, sizeof(txbuff), "mqtt tx");
    fifo_init(&rxfifo, rxbuff, sizeof(rxbuff), "mqtt rx");

    simcom_tcp_register(MQTT_TCP_NUM, &txfifo, &rxfifo);
}

extern uint8_t __IO g_bMqttTaskTimeFlag;
void mqtt_task()
{
    static enum {
        MQTT_TASK_START = 0,
        MQTT_TASK_TCP_CONNECT,

        MQTT_TASK_MQTT_CONNECT,
    } s_emState = MQTT_TASK_START;

    if(!g_bMqttTaskTimeFlag) {
        return;
    }
    g_bMqttTaskTimeFlag = 0;

    if(!simcom_regist_is_cpl()) {
        s_emState = MQTT_TASK_START;
        return;
    }

    if(simcom_tcp_is_reconnect(MQTT_TCP_NUM)) {
        s_emState = MQTT_TASK_MQTT_CONNECT;
    }

    switch(s_emState) {
        case MQTT_TASK_START:
            s_emState = MQTT_TASK_TCP_CONNECT;
            //break;
        case MQTT_TASK_TCP_CONNECT:
            simcom_tcp_connect(MQTT_TCP_NUM, MQTT_SERVER_ADDR, MQTT_SERVER_PORT);
            break;

        case MQTT_TASK_MQTT_CONNECT:
            
            break;
    }
}
