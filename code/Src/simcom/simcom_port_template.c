/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:30
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:44:29
 */
#include "app.h"

/*- 硬件初始化包括 串口 中断 控制IO 初始化 -*/
void simcom_hw_init(void)
{

}

/*- 电源引脚控制 -*/
void simcom_power_ctl(bool lv)
{
    if (lv) {

    } else {

    }
}

/*- 复位引脚控制 -*/
void simcom_reset_ctl(bool lv)
{
    if (lv) {

    } else {

    }
}

/*- 休眠引脚控制 -*/
void simcom_dtr_ctl(bool lv)
{
    if (lv) {

    } else {

    }
}

/*- pwrkey pin control -*/
void simcom_pwrkey_ctl(bool lv)
{
    if (lv) {
        
    } else {
        
    }
}
/*- status pin check -*/
bool simcom_status_check()
{
    return true;
}

/*- 开启串口发送中断 -*/
void simcom_uart_set_send_int()
{

}

/*- 串口发送中断处理函数 -*/
__INLINE void simcom_uart_tx_handler()
{

}

/*- 串口接收中断处理函数 -*/
__INLINE void simcom_uart_rx_handler()
{

}

/*- 串口中断 -*/
void simcom_uart_interrupt_handler()
{
    simcom_uart_tx_handler();

    simcom_uart_rx_handler();
}
