/*
 * @Author: wudi
 * @Date: 2018-07-04 15:10:52
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-20 15:25:36
 * @History:
 * 2018-07-04 增加FTP功能
 *              增加 SIMCOM_FTP_USE_DELAY 宏定义，控制 FTP 命令的延时
 *              修改 FTP 状态，增加 simcom_ftp_state_config 状态
 * 2018-07-20 更新 ftp 接口 采用更加方便的方式接收数据
 *
 */
#include "simcom.h"

#include "simcom_cb.h"
#include "simcom_port.h"

simcom_t simcom;

static uint8_t rxbuff[SIMCOM_RX_BUFF_SIZE];
static uint8_t txbuff[SIMCOM_TX_BUFF_SIZE];
static uint8_t line_buff[SIMCOM_LINE_BUFF_SIZE];


typedef struct __init_cmd_t
{
    char *cmd;                     // 命令
    char *res;                     // 响应
    uint16_t timeout;              // 超时
    uint8_t retry;                 // 重试
    simcom_rt_t (*presfn)(char *); // 响应数据的处理函数
    simcom_rt_t (*pcplfn)(void);   // 命令执行完成的回调函数
} init_cmd_t;

// NOTE 注意指令顺序
#if 0
static init_cmd_t g_tInitCommandArray[] = {
    { "AT" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, simcom_cplcb_at },

#if SIMCOM_ECHO_MODE
    { "ATE1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#else
    { "ATE0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif

    { "AT+IPR=0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT&W" CRLF, "OK", SIMCOM_TIME(500), 5, NULL },

    { "AT+GSN" CRLF, "OK", SIMCOM_TIME(500), 5, simcom_rescb_imei, NULL },

    { "AT+CPIN?" CRLF, "OK", SIMCOM_TIME(1000), 20, simcom_rescb_cpin, simcom_cplcb_cpin },

    { "AT+ICCID" CRLF, "OK", SIMCOM_TIME(500), 5, simcom_rescb_iccid, NULL },

    { "AT+CIMI" CRLF, "OK", SIMCOM_TIME(500), 5, simcom_rescb_imsi, NULL },

    { "AT+CSQ" CRLF, "OK", SIMCOM_TIME(500), 20, simcom_rescb_csq, NULL },

    // gprs附着 gsm注册 gprs注册
    { "AT+CREG?" CRLF, "OK", SIMCOM_TIME(500), 50, simcom_rescb_creg, NULL },
    { "AT+CGREG?" CRLF, "OK", SIMCOM_TIME(500), 50, simcom_rescb_cgreg, NULL },
    { "AT+CGATT?" CRLF, "OK", SIMCOM_TIME(500), 50, simcom_rescb_cgatt, simcom_cplcb_cgatt },

    //{ "AT+CIPHEAD=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    //{ "AT+CIPSRIP=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    //{ "AT+CIPSHOWTP=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+CIPRXGET=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
//联网配置，如果卡没有插入就配置可能会报错
#if SIMCOM_MUXIP_MODE
    { "AT+CIPMUX=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#else
    { "AT+CIPMUX=0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif
    // 多链路要配置下面的指令
    { "AT+CSTT=\"" APN "\","
      "\"" APN_USER "\",\"" APN_PWD "\"" CRLF,
      "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+CIICR" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+CIFSR" CRLF, NULL, SIMCOM_TIME(500), 5, simcom_rescb_ipaddr, NULL },

#if SIMCOM_ENABLE_FTP
    { "AT+SAPBR=3,1,\"Contype\",\"GPRS\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+SAPBR=3,1,\"APN\",\"CMNET\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+SAPBR=1,1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+SAPBR=2,1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },

    { "AT+FTPCID=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    // 主动模式 以服务器来说主被动
    // { "AT+FTPMODE=0" CRLF, NULL, SIMCOM_TIME(500), 5, simcom_rescb_ipaddr, NULL },
    // 被动模式
    { "AT+FTPMODE=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+FTPTYPE=\"I\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif
#if SIMCOM_USE_SLEEP_MODE
    // 进入休眠模式1
    { "AT+CSCLK=1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif
};

#else
static init_cmd_t g_tInitCommandArray[] = {
    { "AT" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, simcom_cplcb_at },

#if SIMCOM_ECHO_MODE
    { "ATE1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#else
    { "ATE0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif

    { "ATI" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },

    { "AT+CPIN?" CRLF, "OK", SIMCOM_TIME(1000), 20, simcom_rescb_cpin, simcom_cplcb_cpin },

    { "AT+GSN=1" CRLF, "OK", SIMCOM_TIME(1000), 5, simcom_rescb_imei, NULL },

    //{ "AT+ICCID" CRLF, "OK", SIMCOM_TIME(500), 5, simcom_rescb_iccid, NULL },

    { "AT+CIMI" CRLF, "OK", SIMCOM_TIME(500), 5, simcom_rescb_imsi, NULL },

    { "AT+CSQ" CRLF, "OK", SIMCOM_TIME(500), 50, simcom_rescb_csq, NULL },

    // { "AT+CEREG=3" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+CEREG?" CRLF, "OK", SIMCOM_TIME(500), 50, simcom_rescb_cereg, NULL },
    { "AT+CGATT?" CRLF, "OK", SIMCOM_TIME(500), 50, simcom_rescb_cgatt, simcom_cplcb_cgatt },

    { "AT+CMVER" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },

    { "AT+MIPLVER?" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLCREATEEX=\"nbiotbt.heclouds.com\",1," ONENET_AUTH_CODE CRLF, "OK", SIMCOM_TIME(5000), 1, NULL, NULL },

#if USE_OMNA_LWM2M
    // temperature
    { "AT+MIPLADDOBJ=0,3303,2,\"11\",0,0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLDISCOVERRSP=0,3303,1,4,\"5700\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },

    // voltage
    { "AT+MIPLADDOBJ=0,3316,2,\"11\",0,0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLDISCOVERRSP=0,3316,1,4,\"5700\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },

// lon lat Timestamp
#if LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_6_0_1
    { "AT+MIPLADDOBJ=0,6,1,\"1\",0,0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLDISCOVERRSP=0,6,1,3,\"0;1\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5515_5514
    { "AT+MIPLADDOBJ=0,3336,1,\"1\",0,0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLDISCOVERRSP=0,3336,1,14,\"5515;5514;5518\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5513_5514
    { "AT+MIPLADDOBJ=0,3336,1,\"1\",0,0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLDISCOVERRSP=0,3336,1,14,\"5513;5514;5518\"" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif
#else
    { "AT+MIPLADDOBJ=0," TO_STR(APP_OBJID) ",1,\"1\",0,0" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    { "AT+MIPLDISCOVERRSP=0," TO_STR(APP_OBJID) ",1," TO_STR(APP_RESID_LEN) ",\"" TO_STR(APP_RESID_VERSION) ";" TO_STR(APP_RESID_VALID) ";" TO_STR(
        APP_RESID_VBAT) ";" TO_STR(APP_RESID_VSYS) ";" TO_STR(APP_RESID_TEMP) ";" TO_STR(APP_RESID_LON) ";" TO_STR(APP_RESID_LAT) ";" TO_STR(APP_RESID_TIMESTAMP) "\"" CRLF,
      "OK", SIMCOM_TIME(500), 5, NULL, NULL },
#endif

    { "AT+MIPLOPEN=0,3600,30" CRLF, "OK", SIMCOM_TIME(30000), 2, NULL, NULL },

    // demo
    // { "AT+MIPLNOTIFY=0,0,3303,0,5700,4,4,\"29.0\",0,0,1" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    // { "AT+MIPLNOTIFY=0,0,3303,1,5700,4,4,\"30.0\",0,0,2" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },

    // { "AT+MIPLNOTIFY=0,0,3336,0,5514,1,7,\"40.3456\",1,1,3" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
    // { "AT+MIPLNOTIFY=0,0,3336,0,5515,1,8,\"153.0000\",0,0,4" CRLF, "OK", SIMCOM_TIME(500), 5, NULL, NULL },
};
#endif


// NOTE 这里可能导致异常，因为外部状态机复位而 cnt 没有复位
static bool simcom_uart_send(uint8_t *buff, uint16_t len)
{
    static uint16_t cnt = 0;

    while (1) {
        if (fifo_put(&simcom.txfifo, buff[cnt])) {
            if (++cnt >= len) {
                cnt = 0;
                simcom_uart_set_send_int();
                return true;
            }
        } else {
            simcom_uart_set_send_int();
            break;
        }
    }

    return false;
}

#if 0
static bool simcom_uart_send_line(uint8_t *buff)
{
    static uint16_t cnt = 0;

    while (1) {
        if (buff[cnt] == '\0') {
            cnt = 0;
            simcom_uart_set_send_int();
            return true;
        }
        if (fifo_put(&simcom.txfifo, buff[cnt])) {
            cnt++;
        } else {
            simcom_uart_set_send_int();
            break;
        }
    }

    return false;
}
#endif

// NOTE 注意这里可能需要适配模块
static bool simcom_uart_recv_line(uint8_t *buff)
{
    uint8_t chByte;
    static uint16_t cnt = 0;

    while (1) {
        if (fifo_get(&simcom.rxfifo, &chByte)) {
            if (chByte == '\r') {

            } else if (chByte == '\n') {
                buff[cnt] = '\0';
                if (cnt != 0) {
                    cnt = 0;
                    if (simcom_rt_on_going == simcom_urc_cb((char *)buff)) {
                        return true;
                    }
                }
            } else {
                buff[cnt] = chByte;
                cnt++;
            }
        } else {
            break;
        }
    }

    return false;
}

static bool simcom_uart_recv(uint8_t *buff, uint16_t len)
{
    static uint16_t cnt = 0;
    uint8_t chByte;

    while (1) {
        if (fifo_get(&simcom.rxfifo, &chByte)) {
            buff[cnt] = chByte;
            if (++cnt >= len) {
                cnt = 0;
                return true;
            }
        } else {
            break;
        }
    }

    return false;
}

void simcom_init(void)
{
    // fifo
    fifo_init(&simcom.rxfifo, rxbuff, sizeof(rxbuff), "simcom rx");
    fifo_init(&simcom.txfifo, txbuff, sizeof(txbuff), "simcom tx");

    simcom_hw_init();
}

/**
 * @brief  simcom通用命令函数
 *         发送命令,如果在回显模式,等待回显
 *         检测rescb
 *              如果不为NULL,执行rescb;
 *              如果为NULL,
 *                  检测res是否为NULL
 *                      如果不为NULL,接收res;
 *                      如果为NULL,
 *                          检测cplcb
 *                              如果不为NULL,执行cplcb;
 *                              如果为NULL,直接返回成功
 * @note
 * @param  *cmd:
 * @param  *res:
 * @param  timeout:
 * @param  retry:
 * @param  *rescb:
 * @param  *cplcb:
 * @retval
 */
static simcom_rt_t simcom_general_cmd(char *cmd, char *res, uint16_t timeout, uint8_t retry, void *rescb, void *cplcb)
{
    static enum {
        SIMCOM_GENERAL_CMD_START = 0,
        SIMCOM_GENERAL_CMD_SEND_CMD,
        SIMCOM_GENERAL_CMD_WAIT_ECHO,
        SIMCOM_GENERAL_CMD_WAIT_DATA,
        SIMCOM_GENERAL_CMD_WAIT_RES,
        SIMCOM_GENERAL_CMD_EXECUTE_CPL,
        SIMCOM_GENERAL_CMD_RETRY,
    } s_emState = SIMCOM_GENERAL_CMD_START;
    simcom_rt_t emState;
    static uint16_t s_hwTimeCnt;
    static uint16_t s_hwRetryTimeCnt;
    static uint8_t s_chRetryCnt;
    simcom_rt_t (*resfn)(uint8_t *) = (simcom_rt_t(*)(uint8_t *))rescb;
    simcom_rt_t (*cplfn)(void) = (simcom_rt_t(*)(void))cplcb;

    switch (s_emState) {
        case SIMCOM_GENERAL_CMD_START:
            s_emState = SIMCOM_GENERAL_CMD_SEND_CMD;
            s_chRetryCnt = 0;
            s_hwTimeCnt = 0;
            s_hwRetryTimeCnt = 0;
            // break;
        case SIMCOM_GENERAL_CMD_SEND_CMD:
            if (simcom_uart_send((uint8_t *)cmd, strlen(cmd))) {
#if SIMCOM_ECHO_MODE
                s_emState = SIMCOM_GENERAL_CMD_WAIT_ECHO;
#else
                s_emState = SIMCOM_GENERAL_CMD_WAIT_DATA;
#endif
            }
            break;
        case SIMCOM_GENERAL_CMD_WAIT_ECHO:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, cmd, strlen(cmd) - sizeof(CRLF) - 1)) {
                    s_emState = SIMCOM_GENERAL_CMD_WAIT_DATA;
                    s_hwTimeCnt = 0;
                }
            }
            break;
        case SIMCOM_GENERAL_CMD_WAIT_DATA:
            if (resfn != NULL) {
                if (simcom_uart_recv_line(line_buff)) {
                    emState = resfn(line_buff);
                    if (simcom_rt_cpl == emState) {
                        if (res != NULL) {
                            s_emState = SIMCOM_GENERAL_CMD_WAIT_RES;
                            s_hwTimeCnt = 0;
                        } else if (cplfn != NULL) {
                            s_emState = SIMCOM_GENERAL_CMD_EXECUTE_CPL;
                            s_hwTimeCnt = 0;
                        } else {
                            s_emState = SIMCOM_GENERAL_CMD_START;
                            return simcom_rt_cpl;
                        }
                    } else if (simcom_rt_err == emState) {
                        // NOTE 如果监测到错误，达到重试次数直接返回 error
                        if (++s_chRetryCnt >= retry) {
                            s_chRetryCnt = 0;
                            s_emState = SIMCOM_GENERAL_CMD_START;
                            return simcom_rt_err;
                        } else {
                            s_emState = SIMCOM_GENERAL_CMD_RETRY;
                            s_hwTimeCnt = 0;
                        }
                    }
                }
            } else {
                if (res != NULL) {
                    s_emState = SIMCOM_GENERAL_CMD_WAIT_RES;
                    s_hwTimeCnt = 0;
                } else if (cplfn != NULL) {
                    s_emState = SIMCOM_GENERAL_CMD_EXECUTE_CPL;
                    s_hwTimeCnt = 0;
                } else {
                    s_emState = SIMCOM_GENERAL_CMD_START;
                    return simcom_rt_cpl;
                }
            }
            break;
        case SIMCOM_GENERAL_CMD_WAIT_RES:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, res, strlen(res))) {
                    if (cplfn != NULL) {
                        s_emState = SIMCOM_GENERAL_CMD_EXECUTE_CPL;
                        s_hwTimeCnt = 0;
                    } else {
                        s_emState = SIMCOM_GENERAL_CMD_START;
                        return simcom_rt_cpl;
                    }
                }
            }
            break;
        case SIMCOM_GENERAL_CMD_EXECUTE_CPL:
            emState = cplfn();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_GENERAL_CMD_START;
                s_hwTimeCnt = 0;
                return simcom_rt_cpl;
            } else if (simcom_rt_err == emState) {
                s_emState = SIMCOM_GENERAL_CMD_START;
                return simcom_rt_err;
            }
            break;
        case SIMCOM_GENERAL_CMD_RETRY:
            if (++s_hwRetryTimeCnt >= SIMCOM_TIME(1000)) {
                s_hwRetryTimeCnt = 0;
                s_emState = SIMCOM_GENERAL_CMD_SEND_CMD;
                s_hwTimeCnt = 0;
            }
            break;
    }

    if (s_emState != SIMCOM_GENERAL_CMD_RETRY) {
        if (++s_hwTimeCnt >= timeout) {
            s_hwTimeCnt = 0;
            // if (SIMCOM_GENERAL_CMD_WAIT_ECHO != s_emState) {
            if (++s_chRetryCnt >= retry) {
                s_chRetryCnt = 0;
                s_emState = SIMCOM_GENERAL_CMD_START;
                return simcom_rt_timeout;
            } else {
                s_emState = SIMCOM_GENERAL_CMD_RETRY;
                s_hwTimeCnt = 0;
            }
            // } else {
            //     // 没有回显 返回err
            //     s_chRetryCnt = 0;
            //     s_emState = SIMCOM_GENERAL_CMD_START;
            //     return simcom_rt_err;
            // }
        }
    }

    return simcom_rt_on_going;
}

#if 0
/**
 * @brief  simcom通用发送函数
 * @note
 * @param  *cmd:
 * @param  *res:
 * @param  *sb:
 * @param  sblen:
 * @param  timeout:
 * @param  retry:
 * @retval
 */
static simcom_rt_t simcom_general_send(char *cmd,
                                       char *res,
                                       uint8_t *sb,
                                       uint16_t sblen,
                                       uint16_t timeout,
                                       uint8_t retry)
{
    static enum {
        SIMCOM_GENERAL_SEND_START = 0,
        SIMCOM_GENERAL_SEND_SEND_CMD,
        SIMCOM_GENERAL_SEND_WAIT_ECHO,
        SIMCOM_GENERAL_SEND_SEND_DATA,
        SIMCOM_GENERAL_SEND_WAIT_RES,
        SIMCOM_GENERAL_SEND_RETRY,
    } s_emState = SIMCOM_GENERAL_SEND_START;
    static uint16_t s_hwTimeCnt;
    static uint16_t s_hwRetryTimeCnt;
    static uint16_t s_chRetryCnt;

    switch (s_emState) {
        case SIMCOM_GENERAL_SEND_START:
            s_emState = SIMCOM_GENERAL_SEND_SEND_CMD;
            s_hwTimeCnt = 0;
            s_chRetryCnt = 0;
            s_hwRetryTimeCnt = 0;
            // break;
        case SIMCOM_GENERAL_SEND_SEND_CMD:
            if (simcom_uart_send((uint8_t *)cmd, strlen(cmd))) {
#if SIMCOM_ECHO_MODE
                s_emState = SIMCOM_GENERAL_SEND_WAIT_ECHO;
#else
                s_emState = SIMCOM_GENERAL_SEND_SEND_DATA;
#endif
            }
            break;
        case SIMCOM_GENERAL_SEND_WAIT_ECHO:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, cmd, strlen(cmd) - sizeof(CRLF) - 1)) {
                    s_emState = SIMCOM_GENERAL_SEND_SEND_DATA;
                }
            }
            break;
        case SIMCOM_GENERAL_SEND_SEND_DATA:
            if (simcom_uart_send(sb, sblen)) {
                s_emState = SIMCOM_GENERAL_SEND_WAIT_RES;
            }
            break;
        case SIMCOM_GENERAL_SEND_WAIT_RES:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, res, strlen(res))) {
                    s_emState = SIMCOM_GENERAL_SEND_START;
                    return simcom_rt_cpl;
                }
            }
            break;
        case SIMCOM_GENERAL_SEND_RETRY:
            if (++s_hwRetryTimeCnt >= SIMCOM_TIME(1000)) {
                s_hwRetryTimeCnt = 0;
                s_emState = SIMCOM_GENERAL_SEND_SEND_CMD;
            }
            break;
    }

    if (s_emState != SIMCOM_GENERAL_SEND_RETRY) {
        if (++s_hwTimeCnt >= timeout) {
            s_hwTimeCnt = 0;
            if (++s_chRetryCnt >= retry) {
                s_chRetryCnt = 0;
                s_emState = SIMCOM_GENERAL_SEND_START;
                return simcom_rt_timeout;
            } else {
                s_emState = SIMCOM_GENERAL_SEND_RETRY;
            }
        }
    }

    return simcom_rt_on_going;
}

static simcom_rt_t simcom_general_recv(char *cmd,char *res,uint16_t timeout,uint8_t retry)
{

    return simcom_rt_on_going;
}
#endif

#if SIMCOM_USE_SLEEP_MODE

static simcom_rt_t simcom_exit_sleep()
{
    static uint16_t s_hwTimeCnt = 0;

    simcom_dtr_ctl(false);
    if (++s_hwTimeCnt >= SIMCOM_TIME(100)) {
        s_hwTimeCnt = 0;

        return simcom_rt_cpl;
    }

    return simcom_rt_on_going;
}

static simcom_rt_t simcom_enter_sleep()
{
    simcom_dtr_ctl(true);

    return simcom_rt_cpl;
}

#else

static simcom_rt_t simcom_exit_sleep()
{
    return simcom_rt_cpl;
}

static simcom_rt_t simcom_enter_sleep()
{
    return simcom_rt_cpl;
}

#endif

/**
 * @brief  检查模块是否注册网络成功
 * @note
 * @retval
 */
bool simcom_regist_is_cpl()
{
    if (simcom.state == MODULE_STATE_REGISTER) {
        return true;
    } else {
        return false;
    }
}

static simcom_rt_t simcom_on_off()
{
    static enum {
        SIMCOM_ON_OFF_START = 0,
        SIMCOM_ON_OFF_POWERDOWN,
        SIMCOM_ON_OFF_PWRKEY_DOWN1,
        SIMCOM_ON_OFF_PWRKEY_HIGH,
        SIMCOM_ON_OFF_PWRKEY_DOWN2,
        SIMCOM_ON_OFF_CHECK_STATUS,
    } s_emState = SIMCOM_ON_OFF_START;
    static uint16_t s_hwTimeCnt;

    switch (s_emState) {
        case SIMCOM_ON_OFF_START:
            s_emState = SIMCOM_ON_OFF_POWERDOWN;
            s_hwTimeCnt = 0;
            simcom_power_ctl(false);
            // break;
        case SIMCOM_ON_OFF_POWERDOWN:
            if (++s_hwTimeCnt >= SIMCOM_TIME(1000)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_ON_OFF_PWRKEY_DOWN1;
                simcom_power_ctl(true);
            }
            break;
        case SIMCOM_ON_OFF_PWRKEY_DOWN1:
            simcom_pwrkey_ctl(false);
            if (++s_hwTimeCnt >= SIMCOM_TIME(1000)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_ON_OFF_PWRKEY_HIGH;
            }
            break;
        case SIMCOM_ON_OFF_PWRKEY_HIGH:
            simcom_pwrkey_ctl(true);
            if (++s_hwTimeCnt >= SIMCOM_TIME(SIMCOM_PWRKEY_KEEP_TIME)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_ON_OFF_PWRKEY_DOWN2;
            }
            break;
        case SIMCOM_ON_OFF_PWRKEY_DOWN2:
            simcom_pwrkey_ctl(false);
            if (++s_hwTimeCnt >= SIMCOM_TIME(1000)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_ON_OFF_CHECK_STATUS;
            }
            break;
        case SIMCOM_ON_OFF_CHECK_STATUS:
            if (simcom_status_check()) {
                s_emState = SIMCOM_ON_OFF_START;
                simcom_dtr_ctl(true);
                return simcom_rt_cpl;
            }
            if (++s_hwTimeCnt >= SIMCOM_TIME(SIMCOM_STATUS_WAIT_CHECK_TIME)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_ON_OFF_START;
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

#define SIMCOM_GENERAL_INIT_RESET_FSM()                                                                                \
    do {                                                                                                               \
        s_emState = SIMCOM_GENERAL_INIT_RUN;                                                                           \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_general_init()
{
    static enum {
        SIMCOM_GENERAL_INIT_START = 0,
        SIMCOM_GENERAL_INIT_RUN,
    } s_emState = SIMCOM_GENERAL_INIT_START;
    static uint8_t s_chCommandCnt = 0;
    init_cmd_t *cmd;
    simcom_rt_t emState;

    switch (s_emState) {
        case SIMCOM_GENERAL_INIT_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_chCommandCnt = 0;
                s_emState = SIMCOM_GENERAL_INIT_RUN;
            }
            break;
        case SIMCOM_GENERAL_INIT_RUN:
            cmd = &g_tInitCommandArray[s_chCommandCnt];
            emState = simcom_general_cmd(cmd->cmd, cmd->res, cmd->timeout, cmd->retry, (void *)cmd->presfn,
                                         (void *)cmd->pcplfn);
            if (simcom_rt_cpl == emState) {
                if (++s_chCommandCnt >= sizeof(g_tInitCommandArray) / sizeof(g_tInitCommandArray[0])) {
                    s_chCommandCnt = 0;
                    SIMCOM_GENERAL_INIT_RESET_FSM();
                    return simcom_rt_cpl;
                }
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                simcom_general_init_error_cb();

                s_chCommandCnt = 0;
                SIMCOM_GENERAL_INIT_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

/**
 * @brief  建立tcp/udp连接
 * @note
 * @param  *tcpudp:
 * @param  *addr:
 * @param  *port:
 * @retval
 */
#define SIMCOM_NET_CONNECT_RESET_FSM()                                                                                 \
    do {                                                                                                               \
        s_emState = SIMCOM_NET_CONNECT_START;                                                                          \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_net_connect(uint8_t ch, uint16_t timeout, uint8_t retry)
{
    static enum {
        SIMCOM_NET_CONNECT_START = 0,
        SIMCOM_NET_CONNECT_EXECUTE,
    } s_emState = SIMCOM_NET_CONNECT_START;
    simcom_rt_t emState;

    switch (s_emState) {
        case SIMCOM_NET_CONNECT_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_NET_CONNECT_EXECUTE;
                sprintf(simcom.cmd, "AT+CIPSTART=%d,\"%s\",\"%s\",%s" CRLF, ch, "TCP", simcom.tcpudp[ch].server,
                        simcom.tcpudp[ch].port);
            }
            break;
        case SIMCOM_NET_CONNECT_EXECUTE:
            emState = simcom_general_cmd(simcom.cmd, NULL, timeout, retry, simcom_rescb_netconnect, NULL);
            if (simcom_rt_cpl == emState) {
                SIMCOM_NET_CONNECT_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_NET_CONNECT_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

/**
 * @brief  关闭tcp/udp连接
 * @note
 * @retval
 */
#define SIMCOM_NET_CLOSE_RESET_FSM()                                                                                   \
    do {                                                                                                               \
        s_emState = SIMCOM_NET_CLOSE_START;                                                                            \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_net_close(uint8_t ch, uint16_t timeout, uint8_t retry)
{
    static enum {
        SIMCOM_NET_CLOSE_START = 0,
        SIMCOM_NET_CLOSE_EXECUTE,
    } s_emState = SIMCOM_NET_CLOSE_START;
    simcom_rt_t emState;

    switch (s_emState) {
        case SIMCOM_NET_CLOSE_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_NET_CLOSE_EXECUTE;
                sprintf(simcom.cmd, "AT+CIPCLOSE=0,%d" CRLF, ch);
            }
            break;
        case SIMCOM_NET_CLOSE_EXECUTE:
            emState = simcom_general_cmd(simcom.cmd, NULL, timeout, retry, simcom_rescb_netclose, NULL);
            if (simcom_rt_cpl == emState) {
                SIMCOM_NET_CLOSE_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_NET_CLOSE_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

#define SIMCOM_NET_SEND_RESET_FSM()                                                                                    \
    do {                                                                                                               \
        s_emState = SIMCOM_NET_SEND_START;                                                                             \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_net_send(uint8_t ch, uint16_t timeout, uint8_t retry)
{
    static enum {
        SIMCOM_NET_SEND_START = 0,
        SIMCOM_NET_SEND_SEND_CMD,
        SIMCOM_NET_SEND_WAIT_ECHO,
        SIMCOM_NET_SEND_SEND_DATA,
        SIMCOM_NET_SEND_WAIT_RES,
        SIMCOM_NET_SEND_RETRY,
    } s_emState = SIMCOM_NET_SEND_START;
    static uint16_t s_hwTimeCnt;
    static uint16_t s_hwRetryTimeCnt;
    static uint16_t s_chRetryCnt;

    switch (s_emState) {
        case SIMCOM_NET_SEND_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_NET_SEND_SEND_CMD;
                s_hwTimeCnt = 0;
                s_chRetryCnt = 0;
                s_hwRetryTimeCnt = 0;
                sprintf(simcom.cmd, "AT+CIPSEND=%d,%d" CRLF, ch, simcom.tcpudp[ch].send.len);
            }
            break;
        case SIMCOM_NET_SEND_SEND_CMD:
            if (simcom_uart_send((uint8_t *)simcom.cmd, strlen(simcom.cmd))) {
#if SIMCOM_ECHO_MODE
                s_emState = SIMCOM_NET_SEND_WAIT_ECHO;
#else
                s_emState = SIMCOM_NET_SEND_SEND_DATA;
#endif
            }
            break;
        case SIMCOM_NET_SEND_WAIT_ECHO:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, simcom.cmd, strlen(simcom.cmd) - sizeof(CRLF) - 1)) {
                    s_emState = SIMCOM_NET_SEND_SEND_DATA;
                    s_hwTimeCnt = 0;
                }
            }
            break;
        case SIMCOM_NET_SEND_SEND_DATA:
            if (simcom_uart_send(simcom.buff, simcom.tcpudp[ch].send.len)) {
                s_emState = SIMCOM_NET_SEND_WAIT_RES;
                s_hwTimeCnt = 0;
            }
            break;
        case SIMCOM_NET_SEND_WAIT_RES:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)&line_buff[3], "SEND OK", sizeof("SEND OK") - 1)) {
                    simcom.tcpudp[ch].send.tlen -= simcom.tcpudp[ch].send.len;
                    if (!simcom.tcpudp[ch].send.tlen) {
                        simcom.tcpudp[ch].action.send = 0;
                    }
                    SIMCOM_NET_SEND_RESET_FSM();
                    return simcom_rt_cpl;
                }
            }
            break;
        case SIMCOM_NET_SEND_RETRY:
            if (++s_hwRetryTimeCnt >= SIMCOM_TIME(1000)) {
                s_hwRetryTimeCnt = 0;
                s_emState = SIMCOM_NET_SEND_SEND_CMD;
                s_hwTimeCnt = 0;
            }
            break;
    }

    if (s_emState != SIMCOM_NET_SEND_RETRY) {
        if (++s_hwTimeCnt >= timeout) {
            s_hwTimeCnt = 0;
            if (++s_chRetryCnt >= retry) {
                s_chRetryCnt = 0;
                SIMCOM_NET_SEND_RESET_FSM();
                return simcom_rt_timeout;
            } else {
                s_emState = SIMCOM_NET_SEND_RETRY;
                s_hwTimeCnt = 0;
            }
        }
    }

    return simcom_rt_on_going;
}

/**
 * @brief  等待TCP Server返回数据，检测URC上报，发送读取命令
 * @note
 * @param  *cmd:
 * @param  *res:
 * @param  *rb:
 * @param  *prblen:
 * @param  timeout:
 * @retval
 */
#define SIMCOM_NET_RECV_RESET_FSM()                                                                                    \
    do {                                                                                                               \
        s_emState = SIMCOM_NET_RECV_START;                                                                             \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_net_recv(uint8_t ch, uint16_t timeout)
{
    static enum {
        SIMCOM_NET_RECV_START = 0,
        SIMCOM_NET_RECV_SEND_CMD,
        SIMCOM_NET_RECV_WAIT_ECHO,
        SIMCOM_NET_RECV_WAIT_RECV_HEAD,
        SIMCOM_NET_RECV_WAIT_DATA,
        SIMCOM_NET_RECV_WAIT_RES,
    } s_emState = SIMCOM_NET_RECV_START;
    static uint16_t s_hwTimeCnt = 0;
    uint16_t len;

    switch (s_emState) {
        case SIMCOM_NET_RECV_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_NET_RECV_SEND_CMD;
                len = sizeof(simcom.buff);
                sprintf(simcom.cmd, "AT+CIPRXGET=2,%d,%d" CRLF, ch, len);
                s_hwTimeCnt = 0;
            }
            break;
        case SIMCOM_NET_RECV_SEND_CMD:
            if (simcom_uart_send((uint8_t *)simcom.cmd, strlen(simcom.cmd))) {
#if SIMCOM_ECHO_MODE
                s_emState = SIMCOM_NET_RECV_WAIT_ECHO;
#else
                s_emState = SIMCOM_NET_RECV_WAIT_RECV_HEAD;
#endif
            }
            break;
        case SIMCOM_NET_RECV_WAIT_ECHO:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, simcom.cmd, strlen(simcom.cmd) - sizeof(CRLF) - 1)) {
                    s_emState = SIMCOM_NET_RECV_WAIT_RECV_HEAD;
                    s_hwTimeCnt = 0;
                }
            }
            break;
        case SIMCOM_NET_RECV_WAIT_RECV_HEAD:
#if MODULE_TYPE == MODULE_TYPE_QUECTEL
            // Quectel
            // +QIRD: 119.164.1.36:10000,TCP,34
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, "+QIRD:", sizeof("+QIRD:") - 1)) {
                    sscanf((char *)line_buff, "+QIRD: %*[^,],TCP,%hu", &simcom.tcpudp[ch].recv.len);
                    s_emState = SIMCOM_NET_RECV_WAIT_DATA;
                    s_hwTimeCnt = 0;
                }
            }
#elif MODULE_TYPE == MODULE_TYPE_SIMCOM
            // simcom
            // +CIPRXGET: 2,<id>,<reqlength>,<cnflength>[,<IP ADDRESS>:<PORT>]
            // +CIPRXGET: 2,0,32,0,"47.92.94.115:8085"
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, "+CIPRXGET: 2", sizeof("+CIPRXGET: 2") - 1)) {
                    sscanf((char *)line_buff, "+CIPRXGET: 2,%*[^,],%hu,%hu", &simcom.tcpudp[ch].recv.len,
                           &simcom.tcpudp[ch].recv.tlen);
                    if (simcom.tcpudp[ch].recv.len) {
                        s_emState = SIMCOM_NET_RECV_WAIT_DATA;
                        s_hwTimeCnt = 0;
                    } else {
                        s_emState = SIMCOM_NET_RECV_WAIT_RES;
                        s_hwTimeCnt = 0;
                    }
                }
            }
#endif
            break;
        case SIMCOM_NET_RECV_WAIT_DATA:
            if (simcom_uart_recv(simcom.buff, simcom.tcpudp[ch].recv.len)) {
                s_emState = SIMCOM_NET_RECV_WAIT_RES;
                s_hwTimeCnt = 0;
            }
            break;
        case SIMCOM_NET_RECV_WAIT_RES:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, "OK", sizeof("OK") - 1)) {
#if MODULE_TYPE == MODULE_TYPE_QUECTEL
                    simcom.tcpudp[ch].recv.tlen -= simcom.tcpudp[ch].recv.len;
#endif
                    if (!simcom.tcpudp[ch].recv.tlen) {
                        if (simcom.tcpudp[ch].action.recv) {
                            simcom.tcpudp[ch].action.recv--;
                        }
                    }
                    SIMCOM_NET_RECV_RESET_FSM();
                    return simcom_rt_cpl;
                }
            }
            break;
    }

    if (++s_hwTimeCnt >= timeout) {
        s_hwTimeCnt = 0;
        SIMCOM_NET_RECV_RESET_FSM();
        return simcom_rt_timeout;
    }

    return simcom_rt_on_going;
}

#if SIMCOM_ENABLE_FTP
#define SIMCOM_FTP_LINK_CONFIG_RESET_FSM()                                                                             \
    do {                                                                                                               \
        s_emState = SIMCOM_FTP_LINK_CONFIG_START;                                                                      \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_ftp_link_config()
{
    static enum {
        SIMCOM_FTP_LINK_CONFIG_START = 0,
        SIMCOM_FTP_LINK_CONFIG_SERVER,
        SIMCOM_FTP_LINK_CONFIG_PORT,
        SIMCOM_FTP_LINK_CONFIG_USERNAME,
        SIMCOM_FTP_LINK_CONFIG_PASSWORD,
    } s_emState = SIMCOM_FTP_LINK_CONFIG_START;
    simcom_rt_t emState;

    switch (s_emState) {
        case SIMCOM_FTP_LINK_CONFIG_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_FTP_LINK_CONFIG_SERVER;
                sprintf(simcom.cmd, "AT+FTPSERV=\"%s\"" CRLF, simcom.ftp.server);
            }
            break;
        case SIMCOM_FTP_LINK_CONFIG_SERVER:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_FTP_LINK_CONFIG_PORT;
                sprintf(simcom.cmd, "AT+FTPPORT=%s" CRLF, simcom.ftp.port);

                // s_emState = SIMCOM_FTP_LINK_CONFIG_USERNAME;
                // sprintf(simcom.cmd,"AT+FTPUN=\"%s\"" CRLF,simcom.ftp.username);
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_LINK_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_LINK_CONFIG_PORT:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_FTP_LINK_CONFIG_USERNAME;
                sprintf(simcom.cmd, "AT+FTPUN=\"%s\"" CRLF, simcom.ftp.username);
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_LINK_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_LINK_CONFIG_USERNAME:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_FTP_LINK_CONFIG_PASSWORD;
                sprintf(simcom.cmd, "AT+FTPPW=\"%s\"" CRLF, simcom.ftp.password);
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_LINK_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_LINK_CONFIG_PASSWORD:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                simcom.ftp.action.config = 0;

                SIMCOM_FTP_LINK_CONFIG_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_LINK_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

#define SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM()                                                                           \
    do {                                                                                                               \
        s_emState = SIMCOM_FTP_GETPUT_CONFIG_START;                                                                    \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_ftp_getput_config()
{
    static enum {
        SIMCOM_FTP_GETPUT_CONFIG_START = 0,
        SIMCOM_FTP_GETPUT_CONFIG_FILEPATH,
        SIMCOM_FTP_GETPUT_CONFIG_FILENAME,
        SIMCOM_FTP_GETPUT_CONFIG_OFFSET,
        SIMCOM_FTP_GETPUT_CONFIG_ACTIONCMD,

        SIMCOM_FTP_GET_FILE_SIZE,
    } s_emState = SIMCOM_FTP_GETPUT_CONFIG_START;
    simcom_rt_t emState;
    static uint8_t s_chMode = 0;

    switch (s_emState) {
        case SIMCOM_FTP_GETPUT_CONFIG_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_FTP_GETPUT_CONFIG_FILEPATH;
                if (simcom.ftp.action.get) {
                    s_chMode = 1;
                    sprintf(simcom.cmd, "AT+FTPGETPATH=\"%s\"" CRLF, simcom.ftp.get.filepath);
                } else if (simcom.ftp.action.put) {
                    s_chMode = 0;
                    sprintf(simcom.cmd, "AT+FTPPUTPATH=\"%s\"" CRLF, simcom.ftp.put.filepath);
                }
            }
            break;
        case SIMCOM_FTP_GETPUT_CONFIG_FILEPATH:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_FTP_GETPUT_CONFIG_FILENAME;
                if (s_chMode) {
                    sprintf(simcom.cmd, "AT+FTPGETNAME=\"%s\"" CRLF, simcom.ftp.get.filename);
                } else {
                    sprintf(simcom.cmd, "AT+FTPPUTNAME=\"%s\"" CRLF, simcom.ftp.get.filename);
                }
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_GETPUT_CONFIG_FILENAME:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                if (s_chMode) {
                    // ftp 文件下载时支持断点续传功能
                    sprintf(simcom.cmd, "AT+FTPREST=%d" CRLF, simcom.ftp.get.offset);
                    s_emState = SIMCOM_FTP_GETPUT_CONFIG_OFFSET;
                    s_emState = SIMCOM_FTP_GET_FILE_SIZE;
                } else {
                    // NOTE ftp 文件上传时是否支持断点续传
                    sprintf(simcom.cmd, "AT+FTPREST=%d" CRLF, simcom.ftp.put.offset);

                    s_emState = SIMCOM_FTP_GETPUT_CONFIG_ACTIONCMD;
                    strcpy(simcom.cmd, "AT+FTPPUT=1" CRLF);
                }
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_GET_FILE_SIZE:
            emState = simcom_general_cmd("AT+FTPSIZE" CRLF, NULL, SIMCOM_TIME(5000), 5, simcom_rescb_ftpsize, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_FTP_GETPUT_CONFIG_OFFSET;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_GETPUT_CONFIG_OFFSET:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_FTP_GETPUT_CONFIG_ACTIONCMD;
                if (s_chMode) {
                    strcpy(simcom.cmd, "AT+FTPGET=1" CRLF);
                } else {
                    strcpy(simcom.cmd, "AT+FTPPUT=1" CRLF);
                }
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_FTP_GETPUT_CONFIG_ACTIONCMD:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(500), 5, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                if (s_chMode) {
                    simcom.ftp.action.get = 0;

                    simcom.ftp.get.state = simcom_ftp_state_on_going;
                } else if (simcom.ftp.action.put) {
                    simcom.ftp.action.put = 0;

                    simcom.ftp.put.state = simcom_ftp_state_on_going;
                }

                SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_FTP_GETPUT_CONFIG_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

#define SIMCOM_FTP_RECV_RESET_FSM()                                                                                    \
    do {                                                                                                               \
        s_emState = SIMCOM_FTP_RECV_START;                                                                             \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_ftp_recv(uint16_t timeout)
{
    static enum {
        SIMCOM_FTP_RECV_START = 0,
        SIMCOM_FTP_RECV_SEND_CMD,
        SIMCOM_FTP_RECV_WAIT_ECHO,
        SIMCOM_FTP_RECV_WAIT_HEAD,
        SIMCOM_FTP_RECV_SEND_DATA,
        SIMCOM_FTP_RECV_WAIT_RES,
    } s_emState = SIMCOM_FTP_RECV_START;
    static uint16_t s_hwTimeCnt = 0;

    switch (s_emState) {
        case SIMCOM_FTP_RECV_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_FTP_RECV_SEND_CMD;
                sprintf(simcom.cmd, "AT+FTPGET=2,%d" CRLF, sizeof(simcom.buff));
                s_hwTimeCnt = 0;
            }
            break;
        case SIMCOM_FTP_RECV_SEND_CMD:
            if (simcom_uart_send((uint8_t *)simcom.cmd, strlen(simcom.cmd))) {
#if SIMCOM_ECHO_MODE
                s_emState = SIMCOM_FTP_RECV_WAIT_ECHO;
#else
                s_emState = SIMCOM_FTP_RECV_WAIT_HEAD;
#endif
            }
            break;
        case SIMCOM_FTP_RECV_WAIT_ECHO:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, simcom.cmd, strlen(simcom.cmd) - sizeof(CRLF) - 1)) {
                    s_emState = SIMCOM_FTP_RECV_WAIT_HEAD;
                    s_hwTimeCnt = 0;
                }
            }
            break;
        case SIMCOM_FTP_RECV_WAIT_HEAD:
            // simcom
            // +FTPGET: 2,50
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, "+FTPGET: 2", sizeof("+FTPGET: 2") - 1)) {
                    sscanf((char *)line_buff, "+FTPGET: 2,%hu", &simcom.len);
                    if (simcom.len) {
                        s_emState = SIMCOM_FTP_RECV_SEND_DATA;
                        s_hwTimeCnt = 0;
                    } else {
                        s_emState = SIMCOM_FTP_RECV_WAIT_RES;
                        s_hwTimeCnt = 0;
                    }
                }
            }
            break;
        case SIMCOM_FTP_RECV_SEND_DATA:
            if (simcom_uart_recv(simcom.buff, simcom.len)) {
                s_emState = SIMCOM_FTP_RECV_WAIT_RES;
                s_hwTimeCnt = 0;
            }
            break;
        case SIMCOM_FTP_RECV_WAIT_RES:
            if (simcom_uart_recv_line(line_buff)) {
                if (!strncmp((char *)line_buff, "OK", sizeof("OK") - 1)) {
#if SIMCOM_FTP_USE_DELAY
                    simcom.ftp.get.delay = 0;
#endif
                    if (!simcom.len) {
                        simcom.ftp.action.recv = 0;
                    } else {
                        if (simcom.ftp.get.datafunc) {
                            simcom.ftp.get.datafunc(simcom.buff, simcom.len, simcom.ftp.get.offset);
                        }
                        simcom.ftp.get.offset += simcom.len;
                    }
                    SIMCOM_FTP_RECV_RESET_FSM();
                    return simcom_rt_cpl;
                }
            }
            break;
    }

    if (++s_hwTimeCnt >= timeout) {
        s_hwTimeCnt = 0;
        SIMCOM_FTP_RECV_RESET_FSM();
        return simcom_rt_timeout;
    }

    return simcom_rt_on_going;
}
#endif

#if 0
// 每发送一组语句接收一个 ackid

typedef struct
{
    char **statement;
    uint8_t size;
} lwm2m_statement_t;

char statement1[][100] = {
    "AT+MIPLNOTIFY=0,0,3303,0,5700,4,4,\"%.1f\",0,0,%d" CRLF, // 温度0
};

char statement2[][100] = {
    "AT+MIPLNOTIFY=0,0,3303,1,5700,4,4,\"%.1f\",0,0,%d" CRLF, // 温度1
};

char statement3[][100] = {
    "AT+MIPLNOTIFY=0,0,3316,0,5700,4,4,\"%.2f\",0,0,%d" CRLF, // 电压0
};

char statement4[][100] = {
    "AT+MIPLNOTIFY=0,0,3316,1,5700,4,4,\"%.2f\",0,0,%d" CRLF, // 电压0
};

char statement5[][100] = {
#if LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_6_0_1
    "AT+MIPLNOTIFY=0,0,6,0,0,1,9,\"%.6f\",2,1,%d" CRLF,  // 经度
    "AT+MIPLNOTIFY=0,0,6,0,1,1,10,\"%.6f\",1,2,%d" CRLF, // 纬度
    "AT+MIPLNOTIFY=0,0,6,0,5,3,10,\"%d\",0,0,%d" CRLF,   // 时间戳
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5515_5514
    "AT+MIPLNOTIFY=0,0,3336,0,5515,1,9,\"%.6f\",2,1,%d" CRLF,  // 经度
    "AT+MIPLNOTIFY=0,0,3336,0,5514,1,10,\"%.6f\",1,2,%d" CRLF, // 纬度
    "AT+MIPLNOTIFY=0,0,3336,0,5518,3,10,\"%d\",0,0,%d" CRLF,   // 时间戳
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5513_5514
    "AT+MIPLNOTIFY=0,0,3336,0,5513,1,9,\"%.6f\",2,1,%d" CRLF,  // 经度
    "AT+MIPLNOTIFY=0,0,3336,0,5514,1,10,\"%.6f\",1,2,%d" CRLF, // 纬度
    "AT+MIPLNOTIFY=0,0,3336,0,5518,3,10,\"%d\",0,0,%d" CRLF,   // 时间戳
#endif
};

const lwm2m_statement_t statement[] = {
    { statement1, SIZEOF(statement1) }, { statement2, SIZEOF(statement2) }, { statement3, SIZEOF(statement3) },
    { statement4, SIZEOF(statement4) }, { statement5, SIZEOF(statement5) },
};
#endif

// 发送完所有语句以后接收 ackid

#if USE_OMNA_LWM2M
const char lwm2m_format_string[][100] = {
    "AT+MIPLNOTIFY=0,0,3316,0,5700,4,4,\"%.2f\",0,0,%d" CRLF, // 电池电压
    "AT+MIPLNOTIFY=0,0,3316,1,5700,4,4,\"%.2f\",0,0,%d" CRLF, // 系统电压
    "AT+MIPLNOTIFY=0,0,3303,0,5700,4,4,\"%.1f\",0,0,%d" CRLF, // 环境温度
    "AT+MIPLNOTIFY=0,0,3303,1,5700,4,4,\"%.1f\",0,0,%d" CRLF, // 温度待定

#if LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_6_0_1
    "AT+MIPLNOTIFY=0,0,6,0,0,1,9,\"%.6f\",2,1,%d" CRLF,  // 经度
    "AT+MIPLNOTIFY=0,0,6,0,1,1,10,\"%.6f\",1,2,%d" CRLF, // 纬度
    "AT+MIPLNOTIFY=0,0,6,0,5,3,10,\"%d\",0,0,%d" CRLF,   // 时间戳
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5515_5514
    "AT+MIPLNOTIFY=0,0,3336,0,5515,1,9,\"%.6f\",2,1,%d" CRLF,  // 经度
    "AT+MIPLNOTIFY=0,0,3336,0,5514,1,10,\"%.6f\",1,2,%d" CRLF, // 纬度
    "AT+MIPLNOTIFY=0,0,3336,0,5518,3,10,\"%d\",0,0,%d" CRLF,   // 时间戳
#elif LWM2M_LOCATION_RESID_LAT_LON == LWM2M_LOCATION_3336_5513_5514
    "AT+MIPLNOTIFY=0,0,3336,0,5513,1,9,\"%.6f\",2,1,%d" CRLF,  // 经度
    "AT+MIPLNOTIFY=0,0,3336,0,5514,1,10,\"%.6f\",1,2,%d" CRLF, // 纬度
    "AT+MIPLNOTIFY=0,0,3336,0,5518,3,10,\"%d\",0,0,%d" CRLF,   // 时间戳
#endif
};
#else
const char lwm2m_format_string[][100] = {
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_VERSION) ",3,2,\"%02d\",7,1,%d" CRLF, // 版本
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(
      APP_RESID_VALID) ",3,2,\"%02d\",6,2,%d" CRLF, // 是否有效，防破解
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_VBAT) ",1,4,\"%.2f\",5,2,%d" CRLF,     // 电池电压
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_VSYS) ",1,4,\"%.2f\",4,2,%d" CRLF,     // 系统电压
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_TEMP) ",1,4,\"%.1f\",3,2,%d" CRLF,     // 环境温度
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_LON) ",1,9,\"%.6f\",2,2,%d" CRLF,      // 经度
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_LAT) ",1,10,\"%.6f\",1,2,%d" CRLF,     // 纬度
    "AT+MIPLNOTIFY=0,0," TO_STR(APP_OBJID) ",0," TO_STR(APP_RESID_TIMESTAMP) ",3,10,\"%d\",0,0,%d" CRLF, // 时间戳
};
#endif

upload_res_t uploadRes;

#define SIMCOM_LWM2M_RESET_FSM()                                                                                       \
    do {                                                                                                               \
        s_emState = SIMCOM_LWM2M_START;                                                                                \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_lwm2m(void)
{
    static enum {
        SIMCOM_LWM2M_START = 0,
        SIMCOM_LWM2M_PREPARE,
        SIMCOM_LWM2M_EXEC,
        SIMCOM_LWM2M_ACKID,
    } s_emState = SIMCOM_LWM2M_START;
    simcom_rt_t emState;
    static uint8_t data_cnt;
    static uint16_t s_hwTimeCnt = 0;

    static uint16_t ackid = 1;

    switch (s_emState) {
        case SIMCOM_LWM2M_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_LWM2M_PREPARE;
                data_cnt = 0;
                s_hwTimeCnt = 0;
                simcom.lwm2m.notifyCnt = 0;
            }
            break;
        case SIMCOM_LWM2M_PREPARE:
#if USE_OMNA_LWM2M
            if (data_cnt == 0) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.vbat / 100.0, ackid);
            } else if (data_cnt == 1) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.vsys / 100.0, ackid);
            } else if (data_cnt == 2) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.temperature / 10.0,
                        ackid);
            } else if (data_cnt == 3) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.valid / 10.0, ackid);
            } else if (data_cnt == 4) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.lat, ackid);
            } else if (data_cnt == 5) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.lon, ackid);
            } else if (data_cnt == 6) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.timestamp, ackid);
            }
#else
            if (data_cnt == 0) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.version, ackid);
            } else if (data_cnt == 1) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], 1, ackid);
            } else if (data_cnt == 2) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.vbat / 100.0, ackid);
            } else if (data_cnt == 3) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.vsys / 100.0, ackid);
            } else if (data_cnt == 4) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], (float)run_param.uploadRes.temperature / 10.0,
                        ackid);
            } else if (data_cnt == 5) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.lat, ackid);
            } else if (data_cnt == 6) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.lon, ackid);
            } else if (data_cnt == 7) {
                sprintf(simcom.cmd, lwm2m_format_string[data_cnt], run_param.uploadRes.timestamp, ackid);
            }
#endif
            s_emState = SIMCOM_LWM2M_EXEC;
            break;
        case SIMCOM_LWM2M_EXEC:
            emState = simcom_general_cmd(simcom.cmd, "OK", SIMCOM_TIME(5000), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                ackid++;
                if (++data_cnt >= SIZEOF(lwm2m_format_string)) {
                    s_emState = SIMCOM_LWM2M_ACKID;
                } else {
                    s_emState = SIMCOM_LWM2M_PREPARE;
                }
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_LWM2M_ACKID:
            simcom_uart_recv_line(line_buff);
            
            // +MIPLEVENT:0,26,1
#if USE_OMNA_LWM2M
            if (simcom.lwm2m.notifyCnt >= 5) {
#else
            if (simcom.lwm2m.notifyCnt >= 1) {
#endif
                if (run_param.thisTimeUploaded) {
                    save_read_cpl();
                } else {
                    run_param.thisTimeUploaded = 1;
                }
                if (save_readable()) {
                    if (save_read((uint8_t *)&uploadRes, sizeof(upload_res_t))) {
                        memcpy((uint8_t *)&run_param.uploadRes, (uint8_t *)&uploadRes, sizeof(upload_res_t));
                        SIMCOM_LWM2M_RESET_FSM();
                        return simcom_rt_on_going;
                    }
                    save_read_cpl();
                }
                SIMCOM_LWM2M_RESET_FSM();
                return simcom_rt_cpl;
            }

            if (++s_hwTimeCnt >= SIMCOM_TIME(30000)) {
                s_hwTimeCnt = 0;
                SIMCOM_LWM2M_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
    }

    return simcom_rt_on_going;
}

#define SIMCOM_LWM2M_LOGOUT_RESET_FSM()                                                                                \
    do {                                                                                                               \
        s_emState = SIMCOM_LWM2M_LOGOUT_START;                                                                         \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_lwm2m_logout(void)
{
    static enum {
        SIMCOM_LWM2M_LOGOUT_START = 0,
        SIMCOM_LWM2M_LOGOUT_CLOSE,
        SIMCOM_LWM2M_LOGOUT_WAIT_CLOSE,
        SIMCOM_LWM2M_LOGOUT_DELOBJ_1,
        SIMCOM_LWM2M_LOGOUT_DELOBJ_2,
        SIMCOM_LWM2M_LOGOUT_DELOBJ_3,
        SIMCOM_LWM2M_LOGOUT_DELETE,
    } s_emState = SIMCOM_LWM2M_LOGOUT_START;
    simcom_rt_t emState;
    static uint16_t s_hwTimeCnt = 0;

    switch (s_emState) {
        case SIMCOM_LWM2M_LOGOUT_START:
            s_emState = SIMCOM_LWM2M_LOGOUT_CLOSE;
            s_hwTimeCnt = 0;
            // break;
        case SIMCOM_LWM2M_LOGOUT_CLOSE:
            emState = simcom_general_cmd("AT+MIPLCLOSE=0" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_LWM2M_LOGOUT_WAIT_CLOSE;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
        case SIMCOM_LWM2M_LOGOUT_WAIT_CLOSE:
            if (simcom_uart_recv_line(line_buff)) {
                // +MIPLEVENT:0,15
                if (!strncmp((char *)line_buff, "+MIPLEVENT:0,15", sizeof("+MIPLEVENT:0,15") - 1)) {
                    s_emState = SIMCOM_LWM2M_LOGOUT_DELOBJ_1;
                }
            }

            if (++s_hwTimeCnt >= SIMCOM_TIME(10000)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_LWM2M_LOGOUT_DELOBJ_1;
            }
            break;
#if USE_OMNA_LWM2M
        case SIMCOM_LWM2M_LOGOUT_DELOBJ_1:
            emState = simcom_general_cmd("AT+MIPLDELOBJ=0,3316" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_LWM2M_LOGOUT_DELOBJ_2;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
        case SIMCOM_LWM2M_LOGOUT_DELOBJ_2:
            emState = simcom_general_cmd("AT+MIPLDELOBJ=0,3303" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_LWM2M_LOGOUT_DELOBJ_3;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
        case SIMCOM_LWM2M_LOGOUT_DELOBJ_3:
            emState = simcom_general_cmd("AT+MIPLDELOBJ=0,3336" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_LWM2M_LOGOUT_DELETE;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
#else
        case SIMCOM_LWM2M_LOGOUT_DELOBJ_1:
            emState =
              simcom_general_cmd("AT+MIPLDELOBJ=0," TO_STR(APP_OBJID) "" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_LWM2M_LOGOUT_DELETE;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
#endif
        case SIMCOM_LWM2M_LOGOUT_DELETE:
            emState = simcom_general_cmd("AT+MIPLDELETE=0" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_LWM2M_LOGOUT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
    }

    return simcom_rt_on_going;
}

#define SIMCOM_AT_CHECK_AT_RESET_FSM()                                                                                 \
    do {                                                                                                               \
        s_emState = SIMCOM_AT_CHECK_START;                                                                             \
        simcom_enter_sleep();                                                                                          \
    } while (0)
static simcom_rt_t simcom_at_check()
{
    static enum {
        SIMCOM_AT_CHECK_START = 0,
        SIMCOM_AT_CHECK_AT,
        SIMCOM_AT_CHECK_CSQ,
        SIMCOM_AT_CHECK_CGATT,
        SIMCOM_AT_CHECK_STATUS,
    } s_emState = SIMCOM_AT_CHECK_START;
    simcom_rt_t emState;

    switch (s_emState) {
        case SIMCOM_AT_CHECK_START:
            if (simcom_rt_cpl == simcom_exit_sleep()) {
                s_emState = SIMCOM_AT_CHECK_AT;
            }
            break;
        case SIMCOM_AT_CHECK_AT:
            emState = simcom_general_cmd("AT" CRLF, "OK", SIMCOM_TIME(500), 3, NULL, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_AT_CHECK_CSQ;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
        case SIMCOM_AT_CHECK_CSQ:
            emState = simcom_general_cmd("AT+CSQ" CRLF, "OK", SIMCOM_TIME(500), 3, simcom_rescb_csq, NULL);
            if (simcom_rt_cpl == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState || simcom_rt_err == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_timeout;
            }
            break;
        case SIMCOM_AT_CHECK_CGATT:
            emState = simcom_general_cmd("AT+CGATT?" CRLF, "OK", SIMCOM_TIME(500), 3, simcom_rescb_cgatt, NULL);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_AT_CHECK_STATUS;
                // return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_timeout;
            } else if (simcom_rt_err == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
        case SIMCOM_AT_CHECK_STATUS:
            emState = simcom_general_cmd("AT+CIPSTATUS" CRLF, NULL, SIMCOM_TIME(500), 3, simcom_rescb_cipstatus, NULL);
            if (simcom_rt_cpl == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_cpl;
            } else if (simcom_rt_timeout == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_timeout;
            } else if (simcom_rt_err == emState) {
                SIMCOM_AT_CHECK_AT_RESET_FSM();
                return simcom_rt_err;
            }
            break;
    }

    return simcom_rt_on_going;
}

static void simcom_struct_init()
{
    simcom.state = MODULE_STATE_NONE;
    for (uint8_t i = 0; i < SIMCOM_TCP_NUM; i++) {
        simcom.tcpudp[i].action.connect = 0;
        simcom.tcpudp[i].action.close = 0;
        simcom.tcpudp[i].action.send = 0;
        simcom.tcpudp[i].action.recv = 0;
        simcom.tcpudp[i].state = TCP_STATE_CLOSED;
    }

    simcom.lwm2m.action.send = 0;
    simcom.lwm2m.action.recv = 0;
    simcom.lwm2m.ackid = 1;
    simcom.lwm2m.cnt = 0;
}

void simcom_task()
{
    static enum {
        SIMCOM_TASK_START = 0,
        SIMCOM_TASK_ON_OFF,
        SIMCOM_TASK_GENERAL_INIT,
        SIMCOM_TASK_IDLE,

        SIMCOM_TASK_NET_CONNECT,
        SIMCOM_TASK_NET_CLOSE,
        SIMCOM_TASK_NET_SEND,
        SIMCOM_TASK_NET_RECV,

#if SIMCOM_ENABLE_FTP
        SIMCOM_TASK_FTP_LINK_CONFIG,
        SIMCOM_TASK_FTP_GETPUT_COFNFIG,
        SIMCOM_TASK_FTP_RECV,
#endif

#if SIMCOM_ENABLE_LWM2M
        SIMCOM_TASK_LWM2M_SEND,
        SIMCOM_TASK_LWM2M_LOGOUT,
        SIMCOM_TASK_LWM2M_END,
#endif

        SIMCOM_TASK_AT_CHECK,

    } s_emState = SIMCOM_TASK_START;
    simcom_rt_t emState;
    static uint16_t s_hwTimeCnt = 0;
    static uint8_t s_chRetryCnt = 0;

    switch (s_emState) {
        case SIMCOM_TASK_START:
            if (!run_param.uploadTaskFlag) {
                break;
            }
#if !(defined DEBUG_MODE)
            if (++s_chRetryCnt > 2) {
                s_emState = SIMCOM_TASK_LWM2M_END;
                break;
            }
#endif
            s_emState = SIMCOM_TASK_ON_OFF;
            simcom_struct_init();
            s_hwTimeCnt = 0;
            break;
        case SIMCOM_TASK_ON_OFF:
            emState = simcom_on_off();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_GENERAL_INIT;
                SIMCOM_INFO("boot success\r\n");
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_START;
                simcom_boot_error_cb();
                SIMCOM_ERROR("boot false\r\n");
            }
            break;
        case SIMCOM_TASK_GENERAL_INIT:
            emState = simcom_general_init();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
                SIMCOM_INFO("general init success\r\n");
#if LED_INDICATE
                HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
                HAL_Delay(BASE_TIMER_TIME(10));
                HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
                HAL_Delay(BASE_TIMER_TIME(100));
                HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
                HAL_Delay(BASE_TIMER_TIME(10));
                HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
#endif
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_START;
                SIMCOM_ERROR("general init false\r\n");
            }
            break;
        case SIMCOM_TASK_IDLE:
            // NOTE 这里需要做任务分发
            // AT指令操作 tcpip建立 断开连接 发送数据 接收数据 发送短信 接收短信 拨打电话 接听电话 dtmf
            // 蓝牙操作

            simcom_uart_recv_line(line_buff);

#if 0
            // 定时检查AT命令，确保模块没有死机 检查信号质量 网络附着情况 检查链路状态 等等
            if (simcom.cmdTimeCnt >= SIMCOM_TIME(60000)) {
                simcom.cmdTimeCnt = 0;
                s_emState = SIMCOM_TASK_AT_CHECK;
                break;
            }
#endif

            if (++s_hwTimeCnt >= SIMCOM_TIME(30000)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_TASK_START;
                break;
            }

            if (simcom.lwm2m.action.send) {
                s_emState = SIMCOM_TASK_LWM2M_SEND;
                break;
            }

            // FTP 相关操作
#if SIMCOM_ENABLE_FTP
            // FTP 获取数据
            if (simcom.ftp.action.config) {
                s_emState = SIMCOM_TASK_FTP_LINK_CONFIG;
                break;
            }

            if (simcom.ftp.action.get || simcom.ftp.action.put) {
                s_emState = SIMCOM_TASK_FTP_GETPUT_COFNFIG;
                break;
            }

            // if (simcom.ftp.action.send) {
            //     s_emState = SIMCOM_TASK_FTP_GETPUT_COFNFIG;
            // }
#if SIMCOM_FTP_USE_DELAY
            if (simcom.ftp.action.recv && (simcom.ftp.get.delay >= SIMCOM_TIME(SIMCOM_FTP_DELAY_TIME))) {
#else
            if (simcom.ftp.action.recv) {
#endif
                s_emState = SIMCOM_TASK_FTP_RECV;
                break;
            }
#endif

#if SIMCOM_ENABLE_NET
            // TCP UDP 相关操作
            for (uint8_t i = 0; i < SIMCOM_TCP_NUM; i++) {
                // 链路打开
                if (simcom.tcpudp[i].action.connect && (simcom.tcpudp[i].state == TCP_STATE_CLOSED)) {
                    s_emState = SIMCOM_TASK_NET_CONNECT;
                    simcom.channel = i;
                    break;
                }

                // 链路关闭
                if (simcom.tcpudp[i].action.close && (simcom.tcpudp[i].state == TCP_STATE_CONNECTED)) {
                    s_emState = SIMCOM_TASK_NET_CLOSE;
                    simcom.channel = i;
                    break;
                }

                // 发送链路数据
                if (simcom.tcpudp[i].action.send && (simcom.tcpudp[i].state == TCP_STATE_CONNECTED)) {
                    s_emState = SIMCOM_TASK_NET_SEND;
                    simcom.channel = i;
#if 1
                    // NOTE 从app缓冲区获取，发送出去，此处应该放在哪里？
                    uint8_t chByte;
                    simcom.tcpudp[i].send.len = sizeof(simcom.buff);
                    for (uint16_t j = 0; j < sizeof(simcom.buff); j++) {
                        if (fifo_get_not_isr(simcom.tcpudp[i].send.fifo, &chByte)) {
                            simcom.buff[j] = chByte;
                        } else {
                            simcom.tcpudp[i].send.len = j;
                            break;
                        }
                    }
#endif
                    break;
                }

                // 接收链路数据
                if (simcom.tcpudp[i].action.recv && (simcom.tcpudp[i].state == TCP_STATE_CONNECTED)) {
                    s_emState = SIMCOM_TASK_NET_RECV;
                    simcom.channel = i;
                    break;
                }
            }
#endif
            break;

#if SIMCOM_ENABLE_NET
        case SIMCOM_TASK_NET_CONNECT:
            emState = simcom_net_connect(simcom.channel, SIMCOM_TIME(10000), 2);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
                SIMCOM_INFO("connect success ip:%s port:%s number:%d\r\n", simcom.tcpudp[simcom.channel].server,
                            simcom.tcpudp[simcom.channel].port, simcom.channel);
            } else if (simcom_rt_timeout == emState) {
                // NOTE 考虑是否链路建立失败重启模块
                s_emState = SIMCOM_TASK_START;
                // s_emState = SIMCOM_TASK_IDLE;
                SIMCOM_ERROR("connect false ip:%s port:%s number:%d\r\n", simcom.tcpudp[simcom.channel].server,
                             simcom.tcpudp[simcom.channel].port, simcom.channel);
            }
            break;
        case SIMCOM_TASK_NET_CLOSE:
            emState = simcom_net_close(simcom.channel, SIMCOM_TIME(500), 2);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
                SIMCOM_INFO("close success number:%d\r\n", simcom.channel);
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_IDLE;
                // TODO 关闭超时以后处理待思考
                simcom.tcpudp[simcom.channel].state = TCP_STATE_CLOSED;
                SIMCOM_ERROR("close false number:%d\r\n", simcom.channel);
            }
            break;
        case SIMCOM_TASK_NET_SEND:
            emState = simcom_net_send(simcom.channel, SIMCOM_TIME(10000), 1);
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
                SIMCOM_INFO("send success number:%d\r\n", simcom.channel);
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_NET_CLOSE;
                SIMCOM_ERROR("send false number:%d\r\n", simcom.channel);
            }
            break;
        case SIMCOM_TASK_NET_RECV:
            emState = simcom_net_recv(simcom.channel, SIMCOM_TIME(1000));
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
#if 1
                // TODO 放到app处理缓冲区中等待处理，此处该放在哪里？
                for (uint16_t i = 0; i < simcom.tcpudp[simcom.channel].recv.len; i++) {
                    fifo_put_not_isr(simcom.tcpudp[simcom.channel].recv.fifo, simcom.buff[i]);
                }
#endif
                SIMCOM_INFO("recv success number:%d\r\n", simcom.channel);
            } else if (simcom_rt_timeout == emState) {
                // TODO 这里错误没有处理
                s_emState = SIMCOM_TASK_IDLE;

                SIMCOM_ERROR("recv false number:%d\r\n", simcom.channel);
            }
            break;
#endif

#if SIMCOM_ENABLE_FTP
        case SIMCOM_TASK_FTP_LINK_CONFIG:
            emState = simcom_ftp_link_config();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            }
            break;
        case SIMCOM_TASK_FTP_GETPUT_COFNFIG:
            emState = simcom_ftp_getput_config();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            }
            break;
        case SIMCOM_TASK_FTP_RECV:
            emState = simcom_ftp_recv(SIMCOM_TIME(500));
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            }
            break;
#endif

        case SIMCOM_TASK_AT_CHECK:
            emState = simcom_at_check();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_IDLE;
            } else if (simcom_rt_err == emState) {
                s_emState = SIMCOM_TASK_START;
            }
            break;

        case SIMCOM_TASK_LWM2M_SEND:
            emState = simcom_lwm2m();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_LWM2M_LOGOUT;
                SIMCOM_INFO("LWM2M send success\r\n");
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_LWM2M_LOGOUT;
                SIMCOM_ERROR("LWM2M send timeout\r\n");
            } else if (simcom_rt_err == emState) {
                s_emState = SIMCOM_TASK_LWM2M_LOGOUT;
                SIMCOM_ERROR("LWM2M send error\r\n");
            }
            break;

        case SIMCOM_TASK_LWM2M_LOGOUT:
            emState = simcom_lwm2m_logout();
            if (simcom_rt_cpl == emState) {
                s_emState = SIMCOM_TASK_LWM2M_END;
                simcom_power_ctl(false);
                SIMCOM_INFO("LWM2M logout success\r\n");
            } else if (simcom_rt_timeout == emState) {
                s_emState = SIMCOM_TASK_LWM2M_END;
                simcom_power_ctl(false);
                SIMCOM_ERROR("LWM2M logout timeout\r\n");
            } else if (simcom_rt_err == emState) {
                s_emState = SIMCOM_TASK_LWM2M_END;
                simcom_power_ctl(false);
                SIMCOM_ERROR("LWM2M logout error\r\n");
            }
            break;

        case SIMCOM_TASK_LWM2M_END:
            if (!run_param.thisTimeUploaded) {
                save_write((uint8_t *)&run_param.uploadRes, sizeof(upload_res_t));
            }
#if !(defined DEBUG_MODE)
#if LED_INDICATE
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
            HAL_Delay(BASE_TIMER_TIME(10));
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
            HAL_Delay(BASE_TIMER_TIME(100));
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
            HAL_Delay(BASE_TIMER_TIME(10));
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
            HAL_Delay(BASE_TIMER_TIME(100));
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_RESET);
            HAL_Delay(BASE_TIMER_TIME(10));
            HAL_GPIO_WritePin(LED_PIN_GPIO_Port, LED_PIN_Pin, GPIO_PIN_SET);
#endif
            Sys_EnterStandbyMode();
#else
            if (++s_hwTimeCnt >= SIMCOM_TIME(20 * 1000)) {
                s_hwTimeCnt = 0;
                s_emState = SIMCOM_TASK_START;
            }
#endif
            break;
    }

    simcom.cmdTimeCnt++;
#if SIMCOM_ENABLE_FTP && SIMCOM_FTP_USE_DELAY
    simcom.ftp.get.delay++;
#endif
}
