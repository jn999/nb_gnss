/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:07
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-20 15:42:45
 */
#ifndef _SIMCOM_CFG_H_
#define _SIMCOM_CFG_H_

#include <stdio.h>
#include <string.h>

#include "debug.h"
#include "dfifo.h"

/*- 目前支持的功能 TCP FTP(仅支持sim800c) http mqtt 接入onenet 阿里云-*/

/*- 使用 电源控制引脚 -*/
#define SIMCOM_USE_POWER_PIN

/*- 使用 复位引脚 -*/
#define SIMCOM_USE_RESET_PIN

/*- pwrkey引脚启动保持时间 ms-*/
#define SIMCOM_PWRKEY_KEEP_TIME (1000)
/*- status引脚等待核实时间 ms-*/
#define SIMCOM_STATUS_WAIT_CHECK_TIME (200)

/*- 串口发送缓冲区大小配置 -*/
#define SIMCOM_TX_BUFF_SIZE (500)
/*- 串口接收缓冲区大小配置 -*/
#define SIMCOM_RX_BUFF_SIZE (1500)
/*- 行缓冲区大小配置 -*/
#define SIMCOM_LINE_BUFF_SIZE (1500)
/*- 发送接收数组大小 -*/
#define SIMCOM_BUFF_SIZE (1024)

/*- 配置发送AT命令带的回车换行样式  可以是"\r\n"或"\r"或"\n" 根据需要配置 -*/
#define CRLF "\r\n"

// 任务触发间隔 1-200ms 推荐 10ms 根据系统选择 其他值可能会有问题，simcom模块反应慢
#define SIMCOM_INTERVAL (10ul)                     // simcom任务时基定义 ms
#define SIMCOM_TIME(__t) ((__t) / SIMCOM_INTERVAL) // 单位 ms

// 功能配置信息
// 模块类型  simcom 和 移远 TODO 中兴 广和通 合宙 有方
#define MODULE_TYPE_SIMCOM 0
#define MODULE_TYPE_QUECTEL 1
#define MODULE_TYPE MODULE_TYPE_SIMCOM

/*- 模块使用睡眠模式 -*/
#define SIMCOM_USE_SLEEP_MODE (0)

// 配置信息
#define SIMCOM_ECHO_MODE (1)  // 是否开启回显模式
#define SIMCOM_MUXIP_MODE (1) // 多链路支持

/*- TCPUDP配置 -*/
#define SIMCOM_ENABLE_NET (0)

/*- LWM2M配置 -*/
#define SIMCOM_ENABLE_LWM2M (1)

/*- FTP相关配置 -*/
#define SIMCOM_ENABLE_FTP (0)       // 支持FTP功能
#define SIMCOM_FTP_USE_DELAY (1)    // FTP下次命令延时一段时间,让其他指令有执行时间
#define SIMCOM_FTP_DELAY_TIME (200) // 延时时间 ms

/*- HTTP相关配置 -*/
#define SIMCOM_ENABLE_HTTP (0) // 支持HTTP功能

/*- MQTT相关配置 -*/
#define SIMCOM_ENABLE_MQTT (0) // 支持MQTT功能

/*- TCP UDP链路配置 -*/
#define SIMCOM_TCP_NUM (2) // tcp链路数目
#define SIMCOM_UDP_NUM (0) // udp链路数目

/*- 额外功能配置 -*/
#define SIMCOM_ENABLE_GNSS (0) // 支持GNSS
#define SIMCOM_ENABLE_LBS (0)  // 基站定位相关
#define SIMCOM_ENABLE_BAT (0)  // 电量检测

/* --------------------------------------------------------------------------------------------------------------------
 */
//配置接入点 中国移动cmnet  中国联通uninet
#define APN "CMNET"
#define APN_USER ""
#define APN_PWD ""

//配置时间服务器
#define NTP_SERVER "210.72.145.44"
#define NTP_PORT "123"


/* --------------------------------------------------------------------------------------------------------------------
 */
/* - debug等级定义 - */
#define SIMCOM_DEBUG_LEVEL_DEBUG 0
#define SIMCOM_DEBUG_LEVEL_INFO 1
#define SIMCOM_DEBUG_LEVEL_ERROR 2
#define SIMCOM_DEBUG_LEVEL_NONE 3

#define SIMCOM_DEBUG_LEVEL SIMCOM_DEBUG_LEVEL_DEBUG

#if SIMCOM_DEBUG_LEVEL < 3
#define SIMCOM_ERROR(fmt, ...)                                                                                         \
    do {                                                                                                               \
        debug_printf("[simcom error]");                                                                                \
        debug_printf(fmt, ##__VA_ARGS__);                                                                              \
    } while (0)
#else
#define SIMCOM_ERROR(fmt, ...)
#endif

#if SIMCOM_DEBUG_LEVEL < 2
#define SIMCOM_INFO(fmt, ...)                                                                                          \
    do {                                                                                                               \
        debug_printf("[simcom info]");                                                                                 \
        debug_printf(fmt, ##__VA_ARGS__);                                                                              \
    } while (0)
#else
#define SIMCOM_INFO(fmt, ...)
#endif

#if SIMCOM_DEBUG_LEVEL < 1
#define SIMCOM_DEBUG(fmt, ...)                                                                                         \
    do {                                                                                                               \
        debug_printf("[simcom debug]");                                                                                \
        debug_printf(fmt, ##__VA_ARGS__);                                                                              \
    } while (0)
#else
#define SIMCOM_DEBUG(fmt, ...)
#endif

#endif
