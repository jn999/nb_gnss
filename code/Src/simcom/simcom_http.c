/*
 * @Author: wudi 
 * @Date: 2018-07-05 16:46:54 
 * @Last Modified by:   wudi 
 * @Last Modified time: 2018-07-05 16:46:54 
 */
/*
请求头后面有两个 \r\n
GET 请求
GET / HTTP/1.1
Host: 127.0.0.1:5000
Content-Length: 0



GET 响应
HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Content-Length: 12
Server: Werkzeug/0.14.1 Python/3.6.6
Date: Thu, 05 Jul 2018 08:01:30 GMT

Hello World!


POST 请求
POST / HTTP/1.1
Host: 127.0.0.1:5000
Content-Length: 8

nikoladi


POST 响应
HTTP/1.0 200 OK
Content-Type: text/html; charset=utf-8
Content-Length: 12
Server: Werkzeug/0.14.1 Python/3.6.6
Date: Thu, 05 Jul 2018 08:06:00 GMT

Hello World!
*/

