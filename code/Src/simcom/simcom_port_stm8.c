/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:30
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 15:44:29
 */
#include "app.h"

void simcom_hw_init(void)
{
    // pwrkey
    GPIO_Init(GPIOC, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_FAST);
    // rst
    GPIO_Init(GPIOC, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_FAST);
    // status

    // gps ant
    GPIO_Init(GPIOE, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_FAST);

    UART3_DeInit();

    UART3_Init((uint32_t)115200, UART3_WORDLENGTH_8D, UART3_STOPBITS_1, UART3_PARITY_NO, UART3_MODE_TXRX_ENABLE);

    UART3_ITConfig(UART3_IT_RXNE_OR, ENABLE);

    // UART3_ITConfig(UART3_IT_TXE, ENABLE);
    // UART3_ITConfig(UART3_IT_TC, ENABLE);
}

/*- 电源引脚控制 -*/
void simcom_power_ctl(bool lv)
{
    if (lv) {

    } else {
    }
}

/*- 复位引脚控制 -*/
void simcom_reset_ctl(bool lv)
{
    if (lv) {

    } else {
    }
}

/*- 休眠引脚控制 -*/
void simcom_dtr_ctl(bool lv)
{
    if (lv) {

    } else {
    }
}

/*- pwrkey pin control -*/
void simcom_pwrkey_ctl(bool lv)
{
    if (lv) {
        GPIO_WriteHigh(GPIOC, GPIO_PIN_1);
    } else {
        GPIO_WriteLow(GPIOC, GPIO_PIN_1);
    }
}
/*- status pin check -*/
bool simcom_status_check()
{
    return true;
}

void simcom_uart_set_send_int()
{
    UART3_ITConfig(UART3_IT_TXE, ENABLE);
}

__INLINE void simcom_uart_tx_handler()
{
    uint8_t chByte;

    UART3_ITConfig(UART3_IT_TXE, DISABLE);

    if (fifo_get(&simcom.txfifo, &chByte)) {
        UART3_SendData8(chByte);
        UART3_ITConfig(UART3_IT_TXE, ENABLE);
    } else {
    }
}

__INLINE void simcom_uart_rx_handler()
{
    uint8_t chByte;

    chByte = UART3_ReceiveData8();
    fifo_put(&simcom.rxfifo, chByte);
}

// 中断处理函数
void simcom_uart_interrupt_handler()
{
    simcom_uart_tx_handler();

    simcom_uart_rx_handler();
}
