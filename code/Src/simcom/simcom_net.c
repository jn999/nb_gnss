/*
 * @Author: wudi
 * @Date: 2018-07-04 15:08:22
 * @Last Modified by: wudi
 * @Last Modified time: 2018-07-04 16:04:25
 */
#include "simcom_net.h"

void simcom_tcp_register(uint8_t ch, fifo_t *txfifo, fifo_t *rxfifo)
{
    simcom.tcpudp[ch].send.fifo = txfifo;
    simcom.tcpudp[ch].recv.fifo = rxfifo;

    simcom.tcpudp[ch].used = 1;
}

void simcom_tcp_connect(uint8_t ch, char *server, char *port)
{
    strcpy(simcom.tcpudp[ch].server, server);
    strcpy(simcom.tcpudp[ch].port, port);
    simcom.tcpudp[ch].action.connect = 1;
    simcom.tcpudp[ch].action.close = 0;
}

void simcom_tcp_close(uint8_t ch)
{
    simcom.tcpudp[ch].action.close = 1;
    simcom.tcpudp[ch].action.connect = 0;
}

int simcom_tcp_send(uint8_t ch, uint8_t *buff, uint16_t len)
{
    int i = 0;

    for (i = 0; i < len; i++) {
        if (!fifo_put_not_isr(simcom.tcpudp[ch].send.fifo, buff[i])) {
            break;
        }
    }
    if (i) {
        simcom.tcpudp[ch].send.tlen += i;
        simcom.tcpudp[ch].action.send = 1;
        return i;
    }

    return -1;
}

int simcom_tcp_recv(uint8_t ch, uint8_t *buff, uint16_t len)
{
    int i = 0;

    for (i = 0; i < len; i++) {
        if (!fifo_get_not_isr(simcom.tcpudp[ch].recv.fifo, &buff[i])) {
            break;
        }
    }

    if (i) {
        return i;
    }

    return -1;
}

bool simcom_tcp_is_connect(uint8_t ch)
{
    if (simcom.tcpudp[ch].state == TCP_STATE_CONNECTED) {
        return true;
    } else {
        return false;
    }
}

bool simcom_tcp_is_reconnect(uint8_t ch)
{
    if (simcom.tcpudp[ch].reconnect) {
        simcom.tcpudp[ch].reconnect = 0;
        return true;
    } else {
        return false;
    }
}

bool simcom_tcp_is_send_cpl(uint8_t ch)
{
    if (simcom.tcpudp[ch].action.send) {
        return false;
    } else {
        return true;
    }
}
